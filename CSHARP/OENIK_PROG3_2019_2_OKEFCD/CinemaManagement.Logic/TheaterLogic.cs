﻿namespace CinemaManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;

    /// <summary>
    /// Class for all theater logic operations.
    /// </summary>
    public class TheaterLogic : ITheaterLogic
    {
        private ITheaterRepository theaterRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="TheaterLogic"/> class.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        /// <param name="mockedRepository">Repository for testing.</param>
        public TheaterLogic(CinemaDBContext ctx = null, ITheaterRepository mockedRepository = null)
        {
            if (mockedRepository == null)
            {
                this.theaterRepository = new TheaterRepository(ctx);
            }
            else
            {
                this.theaterRepository = mockedRepository;
            }
        }

        /// <inheritdoc/>
        public void CreateTheater(Theater theaterToCreate)
        {
            this.theaterRepository.Create(theaterToCreate);
        }

        /// <inheritdoc/>
        public void DeleteTheater(Theater theaterToDelete)
        {
            this.theaterRepository.Delete(theaterToDelete);
        }

        /// <inheritdoc/>
        public IList<Theater> GetAllTheaters()
        {
            return this.theaterRepository.GetAll().ToList() ?? new List<Theater>();
        }

        /// <inheritdoc/>
        public Theater GetTheaterById(int id)
        {
            return this.theaterRepository.GetOneById(id) ?? new Theater();
        }

        /// <inheritdoc/>
        public void UpdateTheater(Theater theaterToUpdate)
        {
            this.theaterRepository.Update(theaterToUpdate);
        }
    }
}
