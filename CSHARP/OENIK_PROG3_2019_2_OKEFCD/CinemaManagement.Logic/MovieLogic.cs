﻿namespace CinemaManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;
    using Newtonsoft.Json;

    /// <summary>
    /// Class for all movie logic operations.
    /// </summary>
    public class MovieLogic : IMovieLogic
    {
        private IMovieRepository movieRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="MovieLogic"/> class.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        /// <param name="mockedRepository">Repository for testing.</param>
        public MovieLogic(CinemaDBContext ctx = null, IMovieRepository mockedRepository = null)
        {
            if (mockedRepository == null)
            {
                this.movieRepository = new MovieRepository(ctx);
            }
            else
            {
                this.movieRepository = mockedRepository;
            }
        }

        /// <inheritdoc/>
        public void CreateMovie(Movie movieToCreate)
        {
            this.movieRepository.Create(movieToCreate);
        }

        /// <inheritdoc/>
        public void DeleteMovie(Movie movieToDelete)
        {
            this.movieRepository.Delete(movieToDelete);
        }

        /// <inheritdoc/>
        public IList<Movie> GetAllMovies()
        {
            return this.movieRepository.GetAll().OrderBy(x => x.Id).ToList() ?? new List<Movie>();
        }

        /// <inheritdoc/>
        public Movie GetMovieById(int id)
        {
            return this.movieRepository.GetOneById(id) ?? new Movie();
        }

        /// <inheritdoc/>
        public void UpdateMovie(Movie movieToUpdate)
        {
            this.movieRepository.Update(movieToUpdate);
        }

        /// <inheritdoc/>
        public string MovieGenerator()
        {
            WebClient webClient = new WebClient();
            try
            {
                var content = webClient.DownloadString("http://localhost:8080/MovieRankJavaServer/MovieRankServlet");
                var deserializeMovies = JsonConvert.DeserializeObject<List<DeserializedMovie>>(content);

                foreach (var movie in deserializeMovies)
                {
                    this.CreateMovie(new Movie()
                    {
                        Name = movie.Name,
                        Length = new TimeSpan(0, movie.LengthInMinutes, 0),
                        Rank = movie.Rank,
                    });
                }

                return "Movies created successfuly";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
