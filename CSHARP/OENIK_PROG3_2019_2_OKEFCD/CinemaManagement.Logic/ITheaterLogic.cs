﻿namespace CinemaManagement.Logic
{
    using System.Collections.Generic;
    using CinemaManagement.Data;

    /// <summary>
    /// Interface for theater logic.
    /// </summary>
    public interface ITheaterLogic
    {
        /// <summary>
        /// Method to get one theater by it's id.
        /// </summary>
        /// <param name="id">Theater id.</param>
        /// <returns>Wanted theater.</returns>
        Theater GetTheaterById(int id);

        /// <summary>
        /// Method to get all Theaters.
        /// </summary>
        /// <returns>List of all the theaters.</returns>
        IList<Theater> GetAllTheaters();

        /// <summary>
        /// Method to create new theater.
        /// </summary>
        /// <param name="theaterToCreate">Theater to create.</param>
        void CreateTheater(Theater theaterToCreate);

        /// <summary>
        /// method to delete theater.
        /// </summary>
        /// <param name="theaterToDelete">Theater to delete.</param>
        void DeleteTheater(Theater theaterToDelete);

        /// <summary>
        /// Method to update theater.
        /// </summary>
        /// <param name="theaterToUpdate">Theater to update with new values except it's id.</param>
        void UpdateTheater(Theater theaterToUpdate);
    }
}
