﻿namespace CinemaManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;

    /// <summary>
    /// Class for all screening logic operations.
    /// </summary>
    public class ScreeningLogic : IScreeningLogic
    {
        private IScreeningRepository screeningRepository;
        private IMovieRepository movieRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScreeningLogic"/> class.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        /// <param name="mockedScreeningRepository">Screening repository for testing.</param>
        /// <param name="mockedMovieRepository">Movie repository for testing.</param>
        public ScreeningLogic(CinemaDBContext ctx = null, IScreeningRepository mockedScreeningRepository = null, IMovieRepository mockedMovieRepository = null)
        {
            if (mockedScreeningRepository == null)
            {
                this.screeningRepository = new ScreeningRepository(ctx);
                this.movieRepository = new MovieRepository(ctx);
            }
            else
            {
                this.screeningRepository = mockedScreeningRepository;
                this.movieRepository = mockedMovieRepository;
            }
        }

        /// <inheritdoc/>
        public void CreateScreening(Screening screeningToCreate)
        {
            this.screeningRepository.Create(screeningToCreate);
        }

        /// <inheritdoc/>
        public void DeleteScreening(Screening screeningToDelete)
        {
            this.screeningRepository.Delete(screeningToDelete);
        }

        /// <inheritdoc/>
        public IList<Screening> GetAllScreenings()
        {
            return this.screeningRepository.GetAll().ToList() ?? new List<Screening>();
        }

        /// <inheritdoc/>
        public Screening GetScreeningById(int id)
        {
            return this.screeningRepository.GetOneById(id) ?? new Screening();
        }

        /// <inheritdoc/>
        public void UpdateScreening(Screening screeningToUpdate)
        {
            this.screeningRepository.Update(screeningToUpdate);
        }

        /// <inheritdoc/>
        public IEnumerable<ScreeningsOfMovie> GetAllScreeningsDividedByMovie()
        {
            var movieWithScreenings = from movie in this.movieRepository.GetAll()
                    join screening in this.screeningRepository.GetAll()
                    on movie.Id equals screening.MovieId into movieJoinScreening
                    select new ScreeningsOfMovie
                    {
                        Movie = movie,
                        Screenings = movieJoinScreening.ToList(),
                    };

            return movieWithScreenings;
        }
    }
}
