﻿namespace CinemaManagement.Logic
{
    using System.Collections.Generic;
    using CinemaManagement.Data;

    /// <summary>
    /// Class to store object of cinema and it's next week screenings.
    /// </summary>
    public class CinemaNextWeekScreenings
    {
        /// <summary>
        /// Gets or sets Cinema Object.
        /// </summary>
        public Cinema Cinema { get; set; }

        /// <summary>
        /// Gets or sets Screenings this week in this.Cinema.
        /// </summary>
        public IEnumerable<Screening> Screenings { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CinemaNextWeekScreenings)
            {
                CinemaNextWeekScreenings instance = obj as CinemaNextWeekScreenings;
                return this.Cinema == instance.Cinema && this.Screenings == instance.Screenings;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Cinema.GetHashCode() + this.Screenings.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            string toReturn = string.Empty;
            toReturn += $"\nCinema: {this.Cinema.ToString()}\n" +
                        $"\n  Screenings:\n";
            foreach (Screening screening in this.Screenings)
            {
                toReturn += "\n" + screening.ToString() + "\n";
            }

            return toReturn;
        }
    }
}
