﻿namespace CinemaManagement.Logic
{
    using System.Collections.Generic;
    using CinemaManagement.Data;

    /// <summary>
    /// Interface for movie logic.
    /// </summary>
    public interface IMovieLogic
    {
        /// <summary>
        /// Method to get one movie by it's id.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <returns>Wanted movie.</returns>
        Movie GetMovieById(int id);

        /// <summary>
        /// Method to get all Movies.
        /// </summary>
        /// <returns>List of all the movies.</returns>
        IList<Movie> GetAllMovies();

        /// <summary>
        /// Method to create new movie.
        /// </summary>
        /// <param name="movieToCreate">Movie to create.</param>
        void CreateMovie(Movie movieToCreate);

        /// <summary>
        /// method to delete movie.
        /// </summary>
        /// <param name="movieToDelete">Movie to delete.</param>
        void DeleteMovie(Movie movieToDelete);

        /// <summary>
        /// Method to update movie.
        /// </summary>
        /// <param name="movieToUpdate">Movie to update with new values except it's id.</param>
        void UpdateMovie(Movie movieToUpdate);

        /// <summary>
        /// Method to ganarate movies.
        /// </summary>
        /// <returns>Message about weather the operation was successful or not.</returns>
        string MovieGenerator();
    }
}
