﻿namespace CinemaManagement.Logic
{
    /// <summary>
    /// Class for Deserialized movie object.
    /// </summary>
    public class DeserializedMovie
    {
        /// <summary>
        /// Gets or sets movie's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets movie's length in minutes.
        /// </summary>
        public int LengthInMinutes { get; set; }

        /// <summary>
        /// Gets or sets movie's rank.
        /// </summary>
        public double Rank { get; set; }
    }
}
