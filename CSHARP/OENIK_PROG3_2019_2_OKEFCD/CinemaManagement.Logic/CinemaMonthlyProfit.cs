﻿namespace CinemaManagement.Logic
{
    using CinemaManagement.Data;

    /// <summary>
    /// Class to store object of cinema with it's monthly profit.
    /// </summary>
    public class CinemaMonthlyProfit
    {
        /// <summary>
        /// Gets or sets cinema object.
        /// </summary>
        public Cinema Cinema { get; set; }

        /// <summary>
        /// Gets or sets monthly profit of this.Cinema.
        /// </summary>
        public decimal? MonthlyProfit { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is CinemaMonthlyProfit)
            {
                CinemaMonthlyProfit instance = obj as CinemaMonthlyProfit;
                return this.Cinema == instance.Cinema && this.MonthlyProfit - instance.MonthlyProfit == 0;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Cinema.GetHashCode() + this.MonthlyProfit.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"\nCinema Name: {this.Cinema.Name}\n" +
                   $"Last month's profit: {this.MonthlyProfit ?? 0}$\n";
        }
    }
}
