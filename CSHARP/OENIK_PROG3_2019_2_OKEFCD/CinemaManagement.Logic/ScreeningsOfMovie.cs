﻿namespace CinemaManagement.Logic
{
    using System.Collections.Generic;
    using CinemaManagement.Data;

    /// <summary>
    /// Class to store movie and all of it's screenings.
    /// </summary>
    public class ScreeningsOfMovie
    {
        /// <summary>
        /// Gets or sets movie object.
        /// </summary>
        public Movie Movie { get; set; }

        /// <summary>
        /// Gets or sets list of all screenings of this.Movie.
        /// </summary>
        public List<Screening> Screenings { get; set; }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is ScreeningsOfMovie)
            {
                ScreeningsOfMovie instance = obj as ScreeningsOfMovie;
                if (this.Screenings.Count != instance.Screenings.Count)
                {
                    return false;
                }

                foreach (var screening in instance.Screenings)
                {
                    if (!this.Screenings.Contains(screening))
                    {
                        return false;
                    }
                }

                return this.Movie == instance.Movie;
            }
            else
            {
                return false;
            }
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.Movie.GetHashCode() + this.Screenings.GetHashCode();
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            string toReturn = string.Empty;
            toReturn += $"\nMovie: {this.Movie.Name}\n";
            foreach (var screening in this.Screenings)
            {
                toReturn += $"\n  Cinema: {screening.Cinema.Name}" +
                                     $"\n  Date: {screening.GetStartTimeInStringFormat()} - {screening.GetEndTimeInStringFormat()}" +
                                     $"\n  price: {screening.Cinema.GetTicketPriceInStringFormat()}\n";
            }

            return toReturn;
        }
    }
}