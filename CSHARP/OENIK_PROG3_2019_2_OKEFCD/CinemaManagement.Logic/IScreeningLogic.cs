﻿namespace CinemaManagement.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Interface for screening logic.
    /// </summary>
    public interface IScreeningLogic
    {
        /// <summary>
        /// Method to get one screening by it's id.
        /// </summary>
        /// <param name="id">Screening id.</param>
        /// <returns>Wanted screening.</returns>
        Screening GetScreeningById(int id);

        /// <summary>
        /// Method to get all Screenings.
        /// </summary>
        /// <returns>List of all the screenings.</returns>
        IList<Screening> GetAllScreenings();

        /// <summary>
        /// Method to create new screening.
        /// </summary>
        /// <param name="screeningToCreate">Screening to create.</param>
        void CreateScreening(Screening screeningToCreate);

        /// <summary>
        /// method to delete screening.
        /// </summary>
        /// <param name="screeningToDelete">Screening to delete.</param>
        void DeleteScreening(Screening screeningToDelete);

        /// <summary>
        /// Method to update screening.
        /// </summary>
        /// <param name="screeningToUpdate">Screening to update with new values except it's id.</param>
        void UpdateScreening(Screening screeningToUpdate);

        /// <summary>
        /// All screenings divided by movies with dates, cinemas and prices.
        /// </summary>
        /// <returns>All screenings divided by movies with dates, cinemas and prices in string format.</returns>
        IEnumerable<ScreeningsOfMovie> GetAllScreeningsDividedByMovie();
    }
}
