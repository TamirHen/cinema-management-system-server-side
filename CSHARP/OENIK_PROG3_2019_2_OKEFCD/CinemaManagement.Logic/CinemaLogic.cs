﻿namespace CinemaManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;

    /// <summary>
    /// Class for all cinema logic operations.
    /// </summary>
    public class CinemaLogic : ICinemaLogic
    {
        private ICinemaRepository cinemaRepository;
        private IScreeningRepository screeningRepository;
        private ITheaterRepository theaterRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CinemaLogic"/> class.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        /// <param name="cinemaMockedRepository">Cinema repository for testing.</param>
        /// <param name="screeningMockedRepository">Screening repository for testing.</param>
        public CinemaLogic(CinemaDBContext ctx = null, ICinemaRepository cinemaMockedRepository = null, IScreeningRepository screeningMockedRepository = null)
        {
            if (cinemaMockedRepository == null)
            {
                this.cinemaRepository = new CinemaRepository(ctx);
                this.theaterRepository = new TheaterRepository(ctx);
                this.screeningRepository = new ScreeningRepository(ctx);
            }
            else
            {
                this.cinemaRepository = cinemaMockedRepository;
                this.screeningRepository = screeningMockedRepository;
            }
        }

        /// <inheritdoc/>
        public void CreateCinema(Cinema cinemaToCreate)
        {
            this.cinemaRepository.Create(cinemaToCreate);
        }

        /// <inheritdoc/>
        public void DeleteCinema(Cinema cinemaToDelete)
        {
            this.cinemaRepository.Delete(cinemaToDelete);
        }

        /// <inheritdoc/>
        public IList<Cinema> GetAllCinemas()
        {
            return this.cinemaRepository.GetAll().ToList() ?? new List<Cinema>();
        }

        /// <inheritdoc/>
        public Cinema GetCinemaById(int id)
        {
            return this.cinemaRepository.GetOneById(id) ?? new Cinema();
        }

        /// <inheritdoc/>
        public void UpdateCinema(Cinema cinemaToUpdate)
        {
            this.cinemaRepository.Update(cinemaToUpdate);
        }

        /// <inheritdoc/>
        public string GetCinemaInStringFormat(Cinema cinema)
        {
            return $"{cinema.ToString()}\n" +
                $"\n--Theaters:\n{this.theaterRepository.GetAllTheatersInStirngFormat(cinema)}" +
                $"\n--Screenings:\n{this.screeningRepository.GetAllScreeningsInStirngFormat(cinema)}" +
                $"---------------\n";
        }

        /// <inheritdoc/>
        public CinemaNextWeekScreenings GetCinemaWithMostScreeningsOptionsThisWeek()
        {
            var endTime = DateTime.Now.AddDays(7);
            var screeningsThisWeek = from screening in this.screeningRepository.GetAll()
                                     where screening.StartTime > DateTime.Now && screening.StartTime < endTime
                                     select screening;

            var cinemasWithScreeningsOptionsThisWeek = from cinema in this.cinemaRepository.GetAll()
                                                       join screening in screeningsThisWeek
                                                       on cinema.Id equals screening.CinemaId into cinemaJoinScreening
                                                       orderby cinemaJoinScreening.Count() descending
                                                       select new CinemaNextWeekScreenings
                                                       {
                                                           Cinema = cinema,
                                                           Screenings = cinemaJoinScreening,
                                                       };
            int mostScreenigs = cinemasWithScreeningsOptionsThisWeek.Max(x => x.Screenings.Count());
            var cinemaWithMostScreeningsOptionsThisWeek = cinemasWithScreeningsOptionsThisWeek.Where(x => x.Screenings.Count() == mostScreenigs);
            return cinemaWithMostScreeningsOptionsThisWeek.FirstOrDefault();
        }

        /// <inheritdoc/>
        public IEnumerable<CinemaMonthlyProfit> GetMonthlyProfitForAllCinemas()
        {
            var startTime = DateTime.Now.AddMonths(-1);
            var screeningsLastMonth = from screening in this.screeningRepository.GetAll()
                                     where screening.StartTime < DateTime.Now && screening.StartTime > startTime
                                     select screening;

            var cinemasWithMonthlyProfit = from cinema in this.cinemaRepository.GetAll()
                                       join screening in screeningsLastMonth
                                       on cinema.Id equals screening.CinemaId into cinemaJoinScreening
                                       select new CinemaMonthlyProfit
                                       {
                                           Cinema = cinema,
                                           MonthlyProfit = (decimal)cinemaJoinScreening.Sum(x => x.TicketsSold * cinema.TicketPrice),
                                       };
            return cinemasWithMonthlyProfit.ToList() ?? new List<CinemaMonthlyProfit>();
        }
    }
}
