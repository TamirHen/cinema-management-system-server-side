﻿namespace CinemaManagement.Logic
{
    using System.Collections.Generic;
    using CinemaManagement.Data;

    /// <summary>
    /// Interface for cinema logic.
    /// </summary>
    public interface ICinemaLogic
    {
        /// <summary>
        /// Method to get one cinema by it's id.
        /// </summary>
        /// <param name="id">Cinema id.</param>
        /// <returns>Wanted cinema.</returns>
        Cinema GetCinemaById(int id);

        /// <summary>
        /// Method to get all cinemas.
        /// </summary>
        /// <returns>List of all the cinemas.</returns>
        IList<Cinema> GetAllCinemas();

        /// <summary>
        /// Method to create new cinema.
        /// </summary>
        /// <param name="cinemaToCreate">Cinema to create.</param>
        void CreateCinema(Cinema cinemaToCreate);

        /// <summary>
        /// method to delete Cinema.
        /// </summary>
        /// <param name="cinemaToDelete">Cinema to delete.</param>
        void DeleteCinema(Cinema cinemaToDelete);

        /// <summary>
        /// Method to update Cinema.
        /// </summary>
        /// <param name="cinemaToUpdate">Cinema to update with new values except it's id.</param>
        void UpdateCinema(Cinema cinemaToUpdate);

        /// <summary>
        /// Method to display all the cinema's delails include it's theaters and screenings.
        /// </summary>
        /// <param name="cinema">Cinema to display.</param>
        /// <returns>All cinema's deltais in string format.</returns>
        string GetCinemaInStringFormat(Cinema cinema);

        /// <summary>
        /// Method to get list of cinemas and they upcoming screenings this week.
        /// </summary>
        /// <returns>List of cinemas and they upcoming screenings this week.</returns>
        CinemaNextWeekScreenings GetCinemaWithMostScreeningsOptionsThisWeek();

        /// <summary>
        /// Get the monthly profit for one cinema.
        /// </summary>
        /// <param name="cinema">Cinema to check it's monthly profit.</param>
        /// <returns>Monthly profit of one cinema.</returns>
        IEnumerable<CinemaMonthlyProfit> GetMonthlyProfitForAllCinemas();
    }
}
