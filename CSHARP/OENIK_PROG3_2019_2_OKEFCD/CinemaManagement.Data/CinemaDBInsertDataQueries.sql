﻿-- Cinema Table --
INSERT INTO Cinema VALUES(1, 'Cinema City', 10, 'Budapest, WestEnd Mall', 5, '14:00', '22:30');
INSERT INTO Cinema VALUES(2, 'Globus Max', 8.5, 'Budapest, Bécsi út 96', 6, '14:30', '22:00');
INSERT INTO Cinema VALUES( 3, 'Yes Planet', 9, 'Budapest, Váci út 40', 5, '15:00', '23:00');

-- Theater Table --
-- Theaters for Cinema City (id = 1):
INSERT INTO Theater VALUES(1 ,1 ,150 ,1 , 10, 130);
INSERT INTO Theater VALUES(2 ,2 ,130 ,1 , 13, 125);
INSERT INTO Theater VALUES(3 ,3 ,100 ,1 , 10, 110);
INSERT INTO Theater VALUES(4 ,4 ,150 ,1 , 12, 130);
INSERT INTO Theater VALUES(5 ,5 ,200 ,1 , 13, 140);
-- Theaters for Glbus Max (id = 2):
INSERT INTO Theater VALUES(6 ,1 ,100 ,2 , 8, 112);
INSERT INTO Theater VALUES(7 ,2 ,130 ,2 , 10, 125);
INSERT INTO Theater VALUES(8 ,3 ,120 ,2 , 11, 125);
INSERT INTO Theater VALUES(9 ,4 ,150 ,2 , 13, 130);
INSERT INTO Theater VALUES(10 ,5 ,200 ,2 , 15, 135);
INSERT INTO Theater VALUES(11 ,6 ,150 ,2 , 13, 130);
-- Theaters for Yes Planet (id = 3):
INSERT INTO Theater VALUES(12 ,1 ,150 ,3 , 10, 125);
INSERT INTO Theater VALUES(13 ,2 ,130 ,3 , 13, 118);
INSERT INTO Theater VALUES(14 ,3 ,100 ,3 , 10, 107);
INSERT INTO Theater VALUES(15 ,4 ,150 ,3 , 12, 125);
INSERT INTO Theater VALUES(16 ,5 ,180 ,3 , 13, 130);

-- Movie Table --
INSERT INTO Movie VALUES(1, 'Inception', null, '02:28');
INSERT INTO Movie VALUES(2, 'The Godfather', null, '02:55');
INSERT INTO Movie VALUES(3, 'The Dark Knight', null, '03:22');
INSERT INTO Movie VALUES(4, 'Fight Club', null, '03:21');
INSERT INTO Movie VALUES(5, 'Forrest Gump', null, '02:22');
INSERT INTO Movie VALUES(6, 'The Lord of the Rings: The Fellowship of the Ring', null, '02:58');
INSERT INTO Movie VALUES(7, 'The Lord of the Rings: The Two Towers', null, '02:59');
INSERT INTO Movie VALUES(8, 'The Lord of the Rings: The Return of the King', null, '03:21');
INSERT INTO Movie VALUES(9, 'The Shawshank Redemption', null, '02:22');
INSERT INTO Movie VALUES(10, 'Star Wars: Episode V - The Empire Strikes Back', null, '02:04');

-- Screening Table --
-- Screenings for Cinema City (id = 1):
INSERT INTO Screening VALUES(1, 1, 4, 3, '2019-10-21 18:00', 30);
INSERT INTO Screening VALUES(2, 1, 9, 4, '2019-10-21 18:20', 22);
INSERT INTO Screening VALUES(3, 1, 10, 1, '2019-10-21 19:15', 15);
INSERT INTO Screening VALUES(4, 1, 2, 5, '2019-10-21 19:30', 27);
INSERT INTO Screening VALUES(5, 1, 6, 2, '2019-10-21 20:45', 8);

-- Screenings for Globus Max (id = 2):
INSERT INTO Screening VALUES(6, 2, 1, 7, '2019-10-21 18:15', 42);
INSERT INTO Screening VALUES(7, 2, 7, 6, '2019-10-21 18:30', 25);
INSERT INTO Screening VALUES(8, 2, 8, 11, '2019-10-21 19:15', 15);
INSERT INTO Screening VALUES(9, 2, 3, 10, '2019-10-21 20:00', 27);
INSERT INTO Screening VALUES(10, 2, 9, 8, '2019-10-21 21:15', 33);

-- Screenings for Yes Planet (id = 3):
INSERT INTO Screening VALUES(11, 3, 3, 12, '2019-10-21 18:15', 61);
INSERT INTO Screening VALUES(12, 3, 1, 14, '2019-10-21 19:30', 22);
INSERT INTO Screening VALUES(13, 3, 4, 13, '2019-10-21 19:45', 55);
INSERT INTO Screening VALUES(14, 3, 9, 16, '2019-10-21 21:00', 74);
INSERT INTO Screening VALUES(15, 3, 5, 15, '2019-10-21 21:30', 18);