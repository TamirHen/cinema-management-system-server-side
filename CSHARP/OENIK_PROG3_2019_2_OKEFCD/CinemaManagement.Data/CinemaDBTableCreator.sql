﻿--IF OBJECTֹֹֹֹ_ID('Cinema','U') IS NOT NULL DROP TABLE Cinema;
--IF OBJECTֹֹֹֹ_ID('Theater','U') IS NOT NULL DROP TABLE Cinema;
--IF OBJECTֹֹֹֹ_ID('Movie','U') IS NOT NULL DROP TABLE Cinema;
--IF OBJECTֹֹֹֹ_ID('Screening','U') IS NOT NULL DROP TABLE Cinema;

CREATE TABLE Cinema(
	Id INT PRIMARY KEY,
	Name NVARCHAR(100),
	TicketPrice DECIMAL,
	Address NVARCHAR(100),
	NumberOfTheaters INT,
	OpenTime DATETIME2,
	CloseTime DATETIME2
);

CREATE TABLE Theater(
	Id INT PRIMARY KEY,
	Number INT,
	Capacity INT,
	CinemaId INT REFERENCES Cinema(Id),
	NumberOfRows INT,
	ScreenSize INT
);

CREATE TABLE Movie(
	Id INT PRIMARY KEY,
	Name NVARCHAR(100),
	Rank FLOAT,
	Length TIME
);

CREATE TABLE Screening(
	Id INT PRIMARY KEY,
	CinemaId INT REFERENCES Cinema(Id),
	MovieId INT REFERENCES Movie(Id),
	TheaterId INT REFERENCES Theater(Id),
	StartTime DATETIME2,
	TicketsSold INT
);