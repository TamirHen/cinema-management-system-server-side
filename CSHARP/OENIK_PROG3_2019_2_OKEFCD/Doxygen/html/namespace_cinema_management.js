var namespace_cinema_management =
[
    [ "Data", "namespace_cinema_management_1_1_data.html", "namespace_cinema_management_1_1_data" ],
    [ "Logic", "namespace_cinema_management_1_1_logic.html", "namespace_cinema_management_1_1_logic" ],
    [ "Repository", "namespace_cinema_management_1_1_repository.html", "namespace_cinema_management_1_1_repository" ],
    [ "System", "namespace_cinema_management_1_1_system.html", "namespace_cinema_management_1_1_system" ]
];