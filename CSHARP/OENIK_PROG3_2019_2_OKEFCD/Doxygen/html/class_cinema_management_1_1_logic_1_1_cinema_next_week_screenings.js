var class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings =
[
    [ "Equals", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html#a07f3c8d3e069e76aa520802b54c20f4b", null ],
    [ "GetHashCode", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html#a481ab8c22665dd330831e7fc25d1c455", null ],
    [ "ToString", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html#ae9e5be6a76a153cbef2d91b8f0efe3df", null ],
    [ "Cinema", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html#a561830e7468db276cc7b92e5fa751196", null ],
    [ "Screenings", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html#abaa1f61b0a510e2686977219cc57fce4", null ]
];