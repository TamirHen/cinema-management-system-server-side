var searchData=
[
  ['icinemalogic_96',['ICinemaLogic',['../interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html',1,'CinemaManagement::Logic']]],
  ['icinemarepository_97',['ICinemaRepository',['../interface_cinema_management_1_1_repository_1_1_i_cinema_repository.html',1,'CinemaManagement::Repository']]],
  ['id_98',['Id',['../class_cinema_management_1_1_data_1_1_cinema.html#a8662210b19c8aa622962df37ec2c7351',1,'CinemaManagement.Data.Cinema.Id()'],['../class_cinema_management_1_1_data_1_1_movie.html#a057a84180ad57cd1df1ce532fa095b48',1,'CinemaManagement.Data.Movie.Id()'],['../class_cinema_management_1_1_data_1_1_screening.html#ad5386c3d593568be8a6e4685694afe74',1,'CinemaManagement.Data.Screening.Id()'],['../class_cinema_management_1_1_data_1_1_theater.html#a3b22835ca87020e6cda27d11b97ffd31',1,'CinemaManagement.Data.Theater.Id()']]],
  ['imovielogic_99',['IMovieLogic',['../interface_cinema_management_1_1_logic_1_1_i_movie_logic.html',1,'CinemaManagement::Logic']]],
  ['imovierepository_100',['IMovieRepository',['../interface_cinema_management_1_1_repository_1_1_i_movie_repository.html',1,'CinemaManagement::Repository']]],
  ['irepository_101',['IRepository',['../interface_cinema_management_1_1_repository_1_1_i_repository.html',1,'CinemaManagement::Repository']]],
  ['irepository_3c_20cinema_20_3e_102',['IRepository&lt; Cinema &gt;',['../interface_cinema_management_1_1_repository_1_1_i_repository.html',1,'CinemaManagement::Repository']]],
  ['irepository_3c_20movie_20_3e_103',['IRepository&lt; Movie &gt;',['../interface_cinema_management_1_1_repository_1_1_i_repository.html',1,'CinemaManagement::Repository']]],
  ['irepository_3c_20screening_20_3e_104',['IRepository&lt; Screening &gt;',['../interface_cinema_management_1_1_repository_1_1_i_repository.html',1,'CinemaManagement::Repository']]],
  ['irepository_3c_20theater_20_3e_105',['IRepository&lt; Theater &gt;',['../interface_cinema_management_1_1_repository_1_1_i_repository.html',1,'CinemaManagement::Repository']]],
  ['iscreeninglogic_106',['IScreeningLogic',['../interface_cinema_management_1_1_logic_1_1_i_screening_logic.html',1,'CinemaManagement::Logic']]],
  ['iscreeningrepository_107',['IScreeningRepository',['../interface_cinema_management_1_1_repository_1_1_i_screening_repository.html',1,'CinemaManagement::Repository']]],
  ['itheaterlogic_108',['ITheaterLogic',['../interface_cinema_management_1_1_logic_1_1_i_theater_logic.html',1,'CinemaManagement::Logic']]],
  ['itheaterrepository_109',['ITheaterRepository',['../interface_cinema_management_1_1_repository_1_1_i_theater_repository.html',1,'CinemaManagement::Repository']]]
];
