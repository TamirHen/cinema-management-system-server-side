var searchData=
[
  ['screenings_328',['Screenings',['../class_cinema_management_1_1_data_1_1_cinema.html#a358c84442b286eb3aaf89becd7a7165f',1,'CinemaManagement.Data.Cinema.Screenings()'],['../class_cinema_management_1_1_data_1_1_movie.html#a1540842ee476bf1118671d424995a873',1,'CinemaManagement.Data.Movie.Screenings()'],['../class_cinema_management_1_1_data_1_1_theater.html#ae1fad484f8d48405e8c2923b62f2529d',1,'CinemaManagement.Data.Theater.Screenings()'],['../class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html#abaa1f61b0a510e2686977219cc57fce4',1,'CinemaManagement.Logic.CinemaNextWeekScreenings.Screenings()'],['../class_cinema_management_1_1_logic_1_1_screenings_of_movie.html#a48da1e9aa6dcee020370afad023a7f9c',1,'CinemaManagement.Logic.ScreeningsOfMovie.Screenings()']]],
  ['screensize_329',['ScreenSize',['../class_cinema_management_1_1_data_1_1_theater.html#aaf193ce189c6a7c0b44c3c4bc236f294',1,'CinemaManagement::Data::Theater']]],
  ['starttime_330',['StartTime',['../class_cinema_management_1_1_data_1_1_screening.html#addb6746a7c99118a8717818a2269c514',1,'CinemaManagement::Data::Screening']]]
];
