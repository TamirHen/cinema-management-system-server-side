var searchData=
[
  ['modifycinema_289',['ModifyCinema',['../class_cinema_management_1_1_system_1_1_edit_mode.html#a3f3dbbf5d6eaaed3c87fea54b4fca869',1,'CinemaManagement::System::EditMode']]],
  ['movie_290',['Movie',['../class_cinema_management_1_1_data_1_1_movie.html#a37b7a4b10ea74ba7ed7effd128e0d686',1,'CinemaManagement::Data::Movie']]],
  ['moviegenerator_291',['MovieGenerator',['../interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#a553624c1f6b3394a54581a872ed79fcf',1,'CinemaManagement.Logic.IMovieLogic.MovieGenerator()'],['../class_cinema_management_1_1_logic_1_1_movie_logic.html#a29d941650bfb389d60a3a5e2c7c87b73',1,'CinemaManagement.Logic.MovieLogic.MovieGenerator()']]],
  ['movielogic_292',['MovieLogic',['../class_cinema_management_1_1_logic_1_1_movie_logic.html#aab965f9231100fac5247f5ce49cd43f9',1,'CinemaManagement::Logic::MovieLogic']]],
  ['movierepository_293',['MovieRepository',['../class_cinema_management_1_1_repository_1_1_movie_repository.html#a78b0390203ffc041d66ce6d839c3ec1f',1,'CinemaManagement::Repository::MovieRepository']]]
];
