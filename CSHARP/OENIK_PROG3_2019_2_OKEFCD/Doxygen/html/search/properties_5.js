var searchData=
[
  ['name_322',['Name',['../class_cinema_management_1_1_data_1_1_cinema.html#a5b895fa0f7b71745d08c5aee3445d6f3',1,'CinemaManagement.Data.Cinema.Name()'],['../class_cinema_management_1_1_data_1_1_movie.html#ade28b2ef238573e6bb98e33b5e03df00',1,'CinemaManagement.Data.Movie.Name()'],['../class_cinema_management_1_1_logic_1_1_deserialized_movie.html#ae487fa2a6bbc618d7a4fccdfa74b63b0',1,'CinemaManagement.Logic.DeserializedMovie.Name()']]],
  ['number_323',['Number',['../class_cinema_management_1_1_data_1_1_theater.html#a36095e29ec1d5643f453878946502378',1,'CinemaManagement::Data::Theater']]],
  ['numberofrows_324',['NumberOfRows',['../class_cinema_management_1_1_data_1_1_theater.html#a8c31b22c364d654f5db6be1af7c7d6aa',1,'CinemaManagement::Data::Theater']]],
  ['numberoftheaters_325',['NumberOfTheaters',['../class_cinema_management_1_1_data_1_1_cinema.html#adfc19ec174daad957ad0b2f3d135618c',1,'CinemaManagement::Data::Cinema']]]
];
