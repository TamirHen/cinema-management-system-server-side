var interface_cinema_management_1_1_logic_1_1_i_theater_logic =
[
    [ "CreateTheater", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html#a6d0590e007c0683d429bd62c628cf266", null ],
    [ "DeleteTheater", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html#ae1fcc80e84047f41893b28564718fc4c", null ],
    [ "GetAllTheaters", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html#a188d8b0a436da130181e6b2c76a80538", null ],
    [ "GetTheaterById", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html#ac061e00a024aff70b7faa9a8ddbf4846", null ],
    [ "UpdateTheater", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html#aa4d61fd3e4fc5805fd625e380dca974c", null ]
];