var namespace_cinema_management_1_1_system =
[
    [ "CreateMethods", "class_cinema_management_1_1_system_1_1_create_methods.html", null ],
    [ "DisplayAndChooseIndex", "class_cinema_management_1_1_system_1_1_display_and_choose_index.html", null ],
    [ "EditMode", "class_cinema_management_1_1_system_1_1_edit_mode.html", null ],
    [ "Program", "class_cinema_management_1_1_system_1_1_program.html", null ],
    [ "UpdateMethods", "class_cinema_management_1_1_system_1_1_update_methods.html", null ],
    [ "ViewMode", "class_cinema_management_1_1_system_1_1_view_mode.html", null ]
];