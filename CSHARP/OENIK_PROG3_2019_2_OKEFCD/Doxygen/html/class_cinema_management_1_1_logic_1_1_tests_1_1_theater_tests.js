var class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests =
[
    [ "CreateTheaterShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html#a961f3c4f0a36277bdb5b2059aabfc96f", null ],
    [ "DeleteTheaterShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html#af2de0627dbeef805fc942ab9b32506aa", null ],
    [ "GetAllTheatersShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html#a2f1bd403d7c9d52ba7aada7b65388043", null ],
    [ "GetTheaterByIdShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html#a8c00fe311250dfe7e422b67ba991ecce", null ],
    [ "UpdateTheaterShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html#a0e97094ae1363cc6b3b97307099f18c7", null ]
];