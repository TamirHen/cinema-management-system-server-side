var interface_cinema_management_1_1_logic_1_1_i_movie_logic =
[
    [ "CreateMovie", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#a9276d06880b13273f9fbfb4bda2c1f2a", null ],
    [ "DeleteMovie", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#ad0ced70715025256c8bd60285fe729f7", null ],
    [ "GetAllMovies", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#aba8fbbf54502ef8453850c112342f8eb", null ],
    [ "GetMovieById", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#afe5f3dbce365a5793a02139060196f83", null ],
    [ "MovieGenerator", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#a553624c1f6b3394a54581a872ed79fcf", null ],
    [ "UpdateMovie", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html#a942a4bac61abce8eaa201ed69c3c599c", null ]
];