var class_cinema_management_1_1_repository_1_1_screening_repository =
[
    [ "ScreeningRepository", "class_cinema_management_1_1_repository_1_1_screening_repository.html#ac5511fb47888847285c38351ee5fbb0b", null ],
    [ "Create", "class_cinema_management_1_1_repository_1_1_screening_repository.html#a458ad2c6dd3e611b4ef17c54d12f342a", null ],
    [ "Delete", "class_cinema_management_1_1_repository_1_1_screening_repository.html#af31fb8dd9a0979a44e9d299656737adf", null ],
    [ "GetAllScreeningsInStirngFormat", "class_cinema_management_1_1_repository_1_1_screening_repository.html#ad8ef8fd9aa7098f219e8f825fc21a11c", null ],
    [ "GetOneById", "class_cinema_management_1_1_repository_1_1_screening_repository.html#a43380f578f8e53c79abbd2fa7c8dfeb4", null ],
    [ "GetUpcomingWeekScreeningsInStirngFormat", "class_cinema_management_1_1_repository_1_1_screening_repository.html#ab551fbbf1d80f86dc9a08cc140705649", null ],
    [ "Update", "class_cinema_management_1_1_repository_1_1_screening_repository.html#a67bcc7aaf083289a28491edff376e597", null ]
];