var class_cinema_management_1_1_repository_1_1_movie_repository =
[
    [ "MovieRepository", "class_cinema_management_1_1_repository_1_1_movie_repository.html#a78b0390203ffc041d66ce6d839c3ec1f", null ],
    [ "Create", "class_cinema_management_1_1_repository_1_1_movie_repository.html#a379ea1bc760e26904dfce7b897b48265", null ],
    [ "Delete", "class_cinema_management_1_1_repository_1_1_movie_repository.html#ac3b3a0997276c93ecaa5609a314a76b5", null ],
    [ "GetOneById", "class_cinema_management_1_1_repository_1_1_movie_repository.html#a0e0b7491d1f9a070f2a8f51ac042e1e0", null ],
    [ "Update", "class_cinema_management_1_1_repository_1_1_movie_repository.html#a642e04ec21baead6b871648d63996aa0", null ]
];