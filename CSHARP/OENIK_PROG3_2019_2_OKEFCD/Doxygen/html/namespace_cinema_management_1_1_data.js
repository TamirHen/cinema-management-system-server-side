var namespace_cinema_management_1_1_data =
[
    [ "Cinema", "class_cinema_management_1_1_data_1_1_cinema.html", "class_cinema_management_1_1_data_1_1_cinema" ],
    [ "CinemaDBContext", "class_cinema_management_1_1_data_1_1_cinema_d_b_context.html", null ],
    [ "Movie", "class_cinema_management_1_1_data_1_1_movie.html", "class_cinema_management_1_1_data_1_1_movie" ],
    [ "Screening", "class_cinema_management_1_1_data_1_1_screening.html", "class_cinema_management_1_1_data_1_1_screening" ],
    [ "Theater", "class_cinema_management_1_1_data_1_1_theater.html", "class_cinema_management_1_1_data_1_1_theater" ]
];