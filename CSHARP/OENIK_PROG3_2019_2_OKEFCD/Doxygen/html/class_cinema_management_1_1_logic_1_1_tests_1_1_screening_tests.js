var class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests =
[
    [ "CreateScreeningShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#ac9a01b6d75d2831b44fe8e447a64072d", null ],
    [ "DeleteScreeningShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#a8d69c610c398506b95213a036ba41570", null ],
    [ "GetAllScreeningsDividedByMovieShouldReturnIEnumerableScreeningsOfMovieObject", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#a5997ef2c50841361f9ddd952a2ac65dd", null ],
    [ "GetAllScreeningsDividedByMovieShouldReturnTheCorrentNumberOfThisMovieScreenings", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#ae6718280d1b57acd2986d74021a6da1a", null ],
    [ "GetAllScreeningsShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#af04ce04b32659a0edab831df07cb537e", null ],
    [ "GetScreeningByIdShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#a9184318cde91ebf6d7f9d4d3d76dcc58", null ],
    [ "UpdateScreeningShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html#a7525cc5723472fb5794ca3d738a22580", null ]
];