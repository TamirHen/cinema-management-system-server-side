var class_cinema_management_1_1_data_1_1_cinema =
[
    [ "Cinema", "class_cinema_management_1_1_data_1_1_cinema.html#af125acce3cdfadce365d74661a63e4b4", null ],
    [ "GetCloseTimeInStringFormat", "class_cinema_management_1_1_data_1_1_cinema.html#a78a2ab267c9dd4948b57af0f204bc50e", null ],
    [ "GetIdInStringFormat", "class_cinema_management_1_1_data_1_1_cinema.html#a76c3b147cda6dfa2898924bb8ed63bd3", null ],
    [ "GetOpenTimeInStringFormat", "class_cinema_management_1_1_data_1_1_cinema.html#a8d09f0eb3657da694eb3c4d9baf75ec8", null ],
    [ "GetTicketPriceInStringFormat", "class_cinema_management_1_1_data_1_1_cinema.html#ade72401ffea464e1918ebbff58b9d91f", null ],
    [ "ToString", "class_cinema_management_1_1_data_1_1_cinema.html#a3ad9ae6f186c916833bd75f781ed9c0c", null ],
    [ "Address", "class_cinema_management_1_1_data_1_1_cinema.html#aec957ec1a9b71b69c2240a1e2a21e877", null ],
    [ "CloseTime", "class_cinema_management_1_1_data_1_1_cinema.html#a84a787c51bda820fbb76ec72d3f06ed1", null ],
    [ "Id", "class_cinema_management_1_1_data_1_1_cinema.html#a8662210b19c8aa622962df37ec2c7351", null ],
    [ "Name", "class_cinema_management_1_1_data_1_1_cinema.html#a5b895fa0f7b71745d08c5aee3445d6f3", null ],
    [ "NumberOfTheaters", "class_cinema_management_1_1_data_1_1_cinema.html#adfc19ec174daad957ad0b2f3d135618c", null ],
    [ "OpenTime", "class_cinema_management_1_1_data_1_1_cinema.html#a11c03dfa6e366958aa3eac9b452213cc", null ],
    [ "Screenings", "class_cinema_management_1_1_data_1_1_cinema.html#a358c84442b286eb3aaf89becd7a7165f", null ],
    [ "Theaters", "class_cinema_management_1_1_data_1_1_cinema.html#a77a584ca0aa6afabdc2e6a94a03f00c8", null ],
    [ "TicketPrice", "class_cinema_management_1_1_data_1_1_cinema.html#a5de1cffcbf90f6cf6df424eec243006a", null ]
];