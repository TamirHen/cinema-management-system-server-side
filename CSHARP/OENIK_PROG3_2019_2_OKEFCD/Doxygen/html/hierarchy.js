var hierarchy =
[
    [ "CinemaManagement.Data.Cinema", "class_cinema_management_1_1_data_1_1_cinema.html", null ],
    [ "CinemaManagement.Logic.CinemaMonthlyProfit", "class_cinema_management_1_1_logic_1_1_cinema_monthly_profit.html", null ],
    [ "CinemaManagement.Logic.CinemaNextWeekScreenings", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html", null ],
    [ "CinemaManagement.Logic.Tests.CinemaTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html", null ],
    [ "CinemaManagement.System.CreateMethods", "class_cinema_management_1_1_system_1_1_create_methods.html", null ],
    [ "DbContext", null, [
      [ "CinemaManagement.Data::CinemaDBContext", "class_cinema_management_1_1_data_1_1_cinema_d_b_context.html", null ]
    ] ],
    [ "CinemaManagement.Logic.DeserializedMovie", "class_cinema_management_1_1_logic_1_1_deserialized_movie.html", null ],
    [ "CinemaManagement.System.DisplayAndChooseIndex", "class_cinema_management_1_1_system_1_1_display_and_choose_index.html", null ],
    [ "CinemaManagement.System.EditMode", "class_cinema_management_1_1_system_1_1_edit_mode.html", null ],
    [ "CinemaManagement.Logic.ICinemaLogic", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html", [
      [ "CinemaManagement.Logic.CinemaLogic", "class_cinema_management_1_1_logic_1_1_cinema_logic.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "CinemaManagement.Repository.Repository< T >", "class_cinema_management_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "CinemaManagement.Logic.IMovieLogic", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html", [
      [ "CinemaManagement.Logic.MovieLogic", "class_cinema_management_1_1_logic_1_1_movie_logic.html", null ]
    ] ],
    [ "CinemaManagement.Repository.IRepository< T >", "interface_cinema_management_1_1_repository_1_1_i_repository.html", [
      [ "CinemaManagement.Repository.Repository< T >", "class_cinema_management_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "CinemaManagement.Repository.IRepository< Cinema >", "interface_cinema_management_1_1_repository_1_1_i_repository.html", [
      [ "CinemaManagement.Repository.ICinemaRepository", "interface_cinema_management_1_1_repository_1_1_i_cinema_repository.html", [
        [ "CinemaManagement.Repository.CinemaRepository", "class_cinema_management_1_1_repository_1_1_cinema_repository.html", null ]
      ] ]
    ] ],
    [ "CinemaManagement.Repository.IRepository< Movie >", "interface_cinema_management_1_1_repository_1_1_i_repository.html", [
      [ "CinemaManagement.Repository.IMovieRepository", "interface_cinema_management_1_1_repository_1_1_i_movie_repository.html", [
        [ "CinemaManagement.Repository.MovieRepository", "class_cinema_management_1_1_repository_1_1_movie_repository.html", null ]
      ] ]
    ] ],
    [ "CinemaManagement.Repository.IRepository< Screening >", "interface_cinema_management_1_1_repository_1_1_i_repository.html", [
      [ "CinemaManagement.Repository.IScreeningRepository", "interface_cinema_management_1_1_repository_1_1_i_screening_repository.html", [
        [ "CinemaManagement.Repository.ScreeningRepository", "class_cinema_management_1_1_repository_1_1_screening_repository.html", null ]
      ] ]
    ] ],
    [ "CinemaManagement.Repository.IRepository< Theater >", "interface_cinema_management_1_1_repository_1_1_i_repository.html", [
      [ "CinemaManagement.Repository.ITheaterRepository", "interface_cinema_management_1_1_repository_1_1_i_theater_repository.html", [
        [ "CinemaManagement.Repository.TheaterRepository", "class_cinema_management_1_1_repository_1_1_theater_repository.html", null ]
      ] ]
    ] ],
    [ "CinemaManagement.Logic.IScreeningLogic", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html", [
      [ "CinemaManagement.Logic.ScreeningLogic", "class_cinema_management_1_1_logic_1_1_screening_logic.html", null ]
    ] ],
    [ "CinemaManagement.Logic.ITheaterLogic", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html", [
      [ "CinemaManagement.Logic.TheaterLogic", "class_cinema_management_1_1_logic_1_1_theater_logic.html", null ]
    ] ],
    [ "CinemaManagement.Data.Movie", "class_cinema_management_1_1_data_1_1_movie.html", null ],
    [ "CinemaManagement.Logic.Tests.MovieTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html", null ],
    [ "CinemaManagement.System.Program", "class_cinema_management_1_1_system_1_1_program.html", null ],
    [ "CinemaManagement.Repository.Repository< Cinema >", "class_cinema_management_1_1_repository_1_1_repository.html", [
      [ "CinemaManagement.Repository.CinemaRepository", "class_cinema_management_1_1_repository_1_1_cinema_repository.html", null ]
    ] ],
    [ "CinemaManagement.Repository.Repository< Movie >", "class_cinema_management_1_1_repository_1_1_repository.html", [
      [ "CinemaManagement.Repository.MovieRepository", "class_cinema_management_1_1_repository_1_1_movie_repository.html", null ]
    ] ],
    [ "CinemaManagement.Repository.Repository< Screening >", "class_cinema_management_1_1_repository_1_1_repository.html", [
      [ "CinemaManagement.Repository.ScreeningRepository", "class_cinema_management_1_1_repository_1_1_screening_repository.html", null ]
    ] ],
    [ "CinemaManagement.Repository.Repository< Theater >", "class_cinema_management_1_1_repository_1_1_repository.html", [
      [ "CinemaManagement.Repository.TheaterRepository", "class_cinema_management_1_1_repository_1_1_theater_repository.html", null ]
    ] ],
    [ "CinemaManagement.Data.Screening", "class_cinema_management_1_1_data_1_1_screening.html", null ],
    [ "CinemaManagement.Logic.ScreeningsOfMovie", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html", null ],
    [ "CinemaManagement.Logic.Tests.ScreeningTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html", null ],
    [ "CinemaManagement.Data.Theater", "class_cinema_management_1_1_data_1_1_theater.html", null ],
    [ "CinemaManagement.Logic.Tests.TheaterTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html", null ],
    [ "CinemaManagement.System.UpdateMethods", "class_cinema_management_1_1_system_1_1_update_methods.html", null ],
    [ "CinemaManagement.System.ViewMode", "class_cinema_management_1_1_system_1_1_view_mode.html", null ]
];