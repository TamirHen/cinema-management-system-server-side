var class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests =
[
    [ "CreateMovieShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html#ab1d6d8c0803996ecbe6cb2c310ed6f93", null ],
    [ "DeleteMovieShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html#a00605b4d372b268563be309f4f16d1fd", null ],
    [ "GetAllMoviesShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html#a3e931fa7437eb4c7d67ae710f0d09ca8", null ],
    [ "GetMovieByIdShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html#a8ecd9448f6fc10ec20d1f2b6cf00da93", null ],
    [ "UpdateMovieShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html#a9e3dcca3afa8b04450c27faa7939b961", null ]
];