var interface_cinema_management_1_1_logic_1_1_i_screening_logic =
[
    [ "CreateScreening", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html#a1643ac8747b68d2365e330b2731e3799", null ],
    [ "DeleteScreening", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html#a7518a6b9d423942bb56861976a19e706", null ],
    [ "GetAllScreenings", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html#a7f2c46eef1ef971b0b8ab56d6dfba649", null ],
    [ "GetAllScreeningsDividedByMovie", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html#acc86ac64211c254a2b5ec8705b2f459d", null ],
    [ "GetScreeningById", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html#ae4da409c019d0eb106d7d63a09490d04", null ],
    [ "UpdateScreening", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html#a756424fc68b8944a3427fff110031e4f", null ]
];