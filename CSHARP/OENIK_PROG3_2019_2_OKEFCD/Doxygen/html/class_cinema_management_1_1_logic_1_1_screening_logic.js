var class_cinema_management_1_1_logic_1_1_screening_logic =
[
    [ "ScreeningLogic", "class_cinema_management_1_1_logic_1_1_screening_logic.html#a718550a181c77acd91b22d42328221f1", null ],
    [ "CreateScreening", "class_cinema_management_1_1_logic_1_1_screening_logic.html#aab1201c6af6be8b55ae8bf092dcb499b", null ],
    [ "DeleteScreening", "class_cinema_management_1_1_logic_1_1_screening_logic.html#a2d3752ab1b3a003ac552038929061056", null ],
    [ "GetAllScreenings", "class_cinema_management_1_1_logic_1_1_screening_logic.html#acb217458888f8398925d0b9332ad8ee5", null ],
    [ "GetAllScreeningsDividedByMovie", "class_cinema_management_1_1_logic_1_1_screening_logic.html#a40f02667b73049d7d4df34688254aad9", null ],
    [ "GetScreeningById", "class_cinema_management_1_1_logic_1_1_screening_logic.html#a463c42cce24347b94a40be37ca34849a", null ],
    [ "UpdateScreening", "class_cinema_management_1_1_logic_1_1_screening_logic.html#aacfbcd615122414c0d68bb09a8ce6f18", null ]
];