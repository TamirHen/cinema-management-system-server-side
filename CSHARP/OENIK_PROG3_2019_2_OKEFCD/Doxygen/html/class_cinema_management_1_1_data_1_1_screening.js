var class_cinema_management_1_1_data_1_1_screening =
[
    [ "GetEndTimeInStringFormat", "class_cinema_management_1_1_data_1_1_screening.html#a07d4a3fc3fda97cb769c308ed3ba806b", null ],
    [ "GetIdInStringFormat", "class_cinema_management_1_1_data_1_1_screening.html#a7383889dbcd2602bbbc0af0e231be19c", null ],
    [ "GetStartTimeInStringFormat", "class_cinema_management_1_1_data_1_1_screening.html#a000293e784a660c4ac8baa52363bff7b", null ],
    [ "ToString", "class_cinema_management_1_1_data_1_1_screening.html#a490f2c2296108c9b57bec488073e773e", null ],
    [ "Cinema", "class_cinema_management_1_1_data_1_1_screening.html#ab8d801cd2ddee8ebc03b88a2a15278c5", null ],
    [ "CinemaId", "class_cinema_management_1_1_data_1_1_screening.html#a6a200866daf4d845a0e4846a7534607c", null ],
    [ "Id", "class_cinema_management_1_1_data_1_1_screening.html#ad5386c3d593568be8a6e4685694afe74", null ],
    [ "Movie", "class_cinema_management_1_1_data_1_1_screening.html#a50b9951be580d98f5b570a418f349dbf", null ],
    [ "MovieId", "class_cinema_management_1_1_data_1_1_screening.html#a1e5aeb5d76934a54bd8cd689bbf56505", null ],
    [ "StartTime", "class_cinema_management_1_1_data_1_1_screening.html#addb6746a7c99118a8717818a2269c514", null ],
    [ "Theater", "class_cinema_management_1_1_data_1_1_screening.html#a6b022d29c9266fdaff796c0364959045", null ],
    [ "TheaterId", "class_cinema_management_1_1_data_1_1_screening.html#a27145a4f5b91ad67da991573882b999c", null ],
    [ "TicketsSold", "class_cinema_management_1_1_data_1_1_screening.html#aec78059dfaa33dc73133304b4da15fa7", null ]
];