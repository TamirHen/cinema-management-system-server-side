var class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests =
[
    [ "CreateCinemaShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#ae14a514fd88e1a97b26a7e09e233b1d5", null ],
    [ "DeleteCinemaShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#ade24975d463ffdd72ae0a6d88936a609", null ],
    [ "GetAllCinemasShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#a4b2b57666bfda0c2bdba636919b78bf3", null ],
    [ "GetCinemaByIdShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#a2b869e70a89e9f9be30fbbed30eedaa3", null ],
    [ "GetCinemaWithMostScreeningsOptionsThisWeekShouldReturnOnlyScreeningsInTheNext7Days", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#a3b944b0648e95d1c8517415763c8c967", null ],
    [ "GetCinemaWithMostScreeningsOptionsThisWeekShouldReturnTheCinemaWithMostScreeningsInTheNext7Days", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#a9fe7c41f3e7c5c7cb4371a69af56a89b", null ],
    [ "GetMonthlyProfitForAllCinemasShouldReturnCorrectAmountOfProfit", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#a447b808c844820cfa78ddc49eb91ecd5", null ],
    [ "UpdateCinemaShouldBeCalledOnce", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html#a1dd1a0160eb52cf05091ffddb6b26e43", null ]
];