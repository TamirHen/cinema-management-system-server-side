var class_cinema_management_1_1_logic_1_1_screenings_of_movie =
[
    [ "Equals", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html#aeb6837eb6e3eb5d9fdc23534226190a6", null ],
    [ "GetHashCode", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html#af60d97c4dd4f31840c4f5a1567c55a7b", null ],
    [ "ToString", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html#a6177fdd92bd5635f817f9bdc62e88947", null ],
    [ "Movie", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html#a95c0f69ffd87cf74a28b08a927189a18", null ],
    [ "Screenings", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html#a48da1e9aa6dcee020370afad023a7f9c", null ]
];