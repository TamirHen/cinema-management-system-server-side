var class_cinema_management_1_1_data_1_1_theater =
[
    [ "Theater", "class_cinema_management_1_1_data_1_1_theater.html#a6c7a31771c2100c858fd03e6a40a6000", null ],
    [ "GetIdInStringFormat", "class_cinema_management_1_1_data_1_1_theater.html#a8098b69e93dbc536cad01f6433bc9ca0", null ],
    [ "GetNameInStringFormat", "class_cinema_management_1_1_data_1_1_theater.html#a13e09af81865dad4b748f8b645606eac", null ],
    [ "GetScreenSizeInStringFormat", "class_cinema_management_1_1_data_1_1_theater.html#a5bacf653ce3e4ea6d88153426ee8bd31", null ],
    [ "ToString", "class_cinema_management_1_1_data_1_1_theater.html#a97e16e959a6f0075468957b285ce5fd6", null ],
    [ "Capacity", "class_cinema_management_1_1_data_1_1_theater.html#a54738488fdc72f2f9022ce14ea51a79b", null ],
    [ "Cinema", "class_cinema_management_1_1_data_1_1_theater.html#a50f9efa9baca1d0865e75365422bda33", null ],
    [ "CinemaId", "class_cinema_management_1_1_data_1_1_theater.html#a755dba64ebc06584e955cccbf7eb72bf", null ],
    [ "Id", "class_cinema_management_1_1_data_1_1_theater.html#a3b22835ca87020e6cda27d11b97ffd31", null ],
    [ "Number", "class_cinema_management_1_1_data_1_1_theater.html#a36095e29ec1d5643f453878946502378", null ],
    [ "NumberOfRows", "class_cinema_management_1_1_data_1_1_theater.html#a8c31b22c364d654f5db6be1af7c7d6aa", null ],
    [ "Screenings", "class_cinema_management_1_1_data_1_1_theater.html#ae1fad484f8d48405e8c2923b62f2529d", null ],
    [ "ScreenSize", "class_cinema_management_1_1_data_1_1_theater.html#aaf193ce189c6a7c0b44c3c4bc236f294", null ]
];