var class_cinema_management_1_1_data_1_1_movie =
[
    [ "Movie", "class_cinema_management_1_1_data_1_1_movie.html#a37b7a4b10ea74ba7ed7effd128e0d686", null ],
    [ "GetIdInStringFormat", "class_cinema_management_1_1_data_1_1_movie.html#a2e0e8eeb7e1ea7d50287eb046e316de1", null ],
    [ "GetLengthInStringFormat", "class_cinema_management_1_1_data_1_1_movie.html#a46fcb924506fb185cdc9cb9a93a05d5b", null ],
    [ "GetRankInStringFormat", "class_cinema_management_1_1_data_1_1_movie.html#abc788ebc300bc0a184e1a840bd44bd45", null ],
    [ "ToString", "class_cinema_management_1_1_data_1_1_movie.html#aff0d4bceb5f2438da16b22e0bceedc56", null ],
    [ "Id", "class_cinema_management_1_1_data_1_1_movie.html#a057a84180ad57cd1df1ce532fa095b48", null ],
    [ "Length", "class_cinema_management_1_1_data_1_1_movie.html#a0f703abdece4918a94c3c31251f8f7de", null ],
    [ "Name", "class_cinema_management_1_1_data_1_1_movie.html#ade28b2ef238573e6bb98e33b5e03df00", null ],
    [ "Rank", "class_cinema_management_1_1_data_1_1_movie.html#a7235e7a6ebcbec368fbcf79fdf10e5c3", null ],
    [ "Screenings", "class_cinema_management_1_1_data_1_1_movie.html#a1540842ee476bf1118671d424995a873", null ]
];