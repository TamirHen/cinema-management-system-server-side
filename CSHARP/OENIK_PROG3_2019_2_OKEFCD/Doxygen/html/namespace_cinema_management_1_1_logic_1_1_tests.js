var namespace_cinema_management_1_1_logic_1_1_tests =
[
    [ "CinemaTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests.html", "class_cinema_management_1_1_logic_1_1_tests_1_1_cinema_tests" ],
    [ "MovieTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests.html", "class_cinema_management_1_1_logic_1_1_tests_1_1_movie_tests" ],
    [ "ScreeningTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests.html", "class_cinema_management_1_1_logic_1_1_tests_1_1_screening_tests" ],
    [ "TheaterTests", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests.html", "class_cinema_management_1_1_logic_1_1_tests_1_1_theater_tests" ]
];