/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Cinema Management System", "index.html", [
    [ "Castle Core Changelog", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html", [
      [ "4.4.0 (2019-04-05)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md1", null ],
      [ "4.3.1 (2018-06-21)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md2", null ],
      [ "4.3.0 (2018-06-07)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md3", null ],
      [ "4.2.1 (2017-10-11)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md4", null ],
      [ "4.2.0 (2017-09-28)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md5", null ],
      [ "4.1.1 (2017-07-12)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md6", null ],
      [ "4.1.0 (2017-06-11)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md7", null ],
      [ "4.0.0 (2017-01-25)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md8", null ],
      [ "4.0.0-beta002 (2016-10-28)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md9", null ],
      [ "4.0.0-beta001 (2016-07-17)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md10", null ],
      [ "4.0.0-alpha001 (2016-04-07)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md11", null ],
      [ "3.3.3 (2014-11-06)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md12", null ],
      [ "3.3.2 (2014-11-03)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md13", null ],
      [ "3.3.1 (2014-09-10)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md14", null ],
      [ "3.3.0 (2014-04-27)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md15", null ],
      [ "3.2.2 (2013-11-30)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md16", null ],
      [ "3.2.1 (2013-10-05)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md17", null ],
      [ "3.2.0 (2013-02-16)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md18", null ],
      [ "3.1.0 (2012-08-05)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md19", null ],
      [ "3.1.0 RC (2012-07-08)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md20", null ],
      [ "3.0.0 (2011-12-13)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md21", null ],
      [ "3.0.0 RC 1 (2011-11-20)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md22", null ],
      [ "3.0.0 beta 1 (2011-08-14)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md23", null ],
      [ "2.5.2 (2010-11-15)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md24", null ],
      [ "2.5.1 (2010-09-21)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md25", null ],
      [ "2.5.0 (2010-08-21)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md26", null ],
      [ "1.2.0 (2010-01-11)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md27", null ],
      [ "1.2.0 beta (2009-12-04)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md28", null ],
      [ "1.1.0 (2009-05-04)", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md29", null ],
      [ "Release Candidate 3", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md30", null ],
      [ "0.0.1.0", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_d581a448ded8f47447b0fb84273b268e7.html#autotoc_md31", null ]
    ] ],
    [ "LICENSE", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_dcd9a00f33d0c25a9a1f5c9cf9e3fb1cd.html", null ],
    [ "NUnit 3.12 - May 14, 2019", "md____mac__home__desktop__prog3__home_work__c_s_h_a_r_p__o_e_n_i_k__p_r_o_g3_2019_2__o_k_e_f_c_dd30919aced8f2bea0ce86ac4c961f409.html", null ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Properties", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_cinema_8cs_source.html",
"functions_i.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';