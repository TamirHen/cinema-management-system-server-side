var class_cinema_management_1_1_logic_1_1_theater_logic =
[
    [ "TheaterLogic", "class_cinema_management_1_1_logic_1_1_theater_logic.html#aa91e9f00fb29631ad40c5ceaab4613dc", null ],
    [ "CreateTheater", "class_cinema_management_1_1_logic_1_1_theater_logic.html#a01435b9f65a80eaf3404c317b2cc64a0", null ],
    [ "DeleteTheater", "class_cinema_management_1_1_logic_1_1_theater_logic.html#a58aa0ab1e3cc257af3ccc7fd9cbfede9", null ],
    [ "GetAllTheaters", "class_cinema_management_1_1_logic_1_1_theater_logic.html#ac9d924353ac14a486664d67a1fc79ee4", null ],
    [ "GetTheaterById", "class_cinema_management_1_1_logic_1_1_theater_logic.html#a5a98ebf379356c3ef76e654561d1d2b1", null ],
    [ "UpdateTheater", "class_cinema_management_1_1_logic_1_1_theater_logic.html#a44b62197ea455f6a6553693553fb3b15", null ]
];