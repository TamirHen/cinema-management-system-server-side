var interface_cinema_management_1_1_logic_1_1_i_cinema_logic =
[
    [ "CreateCinema", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#a52903c15689991c27c73b8801bd3a04e", null ],
    [ "DeleteCinema", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#a4ca55df82a7cec53f56340e69c51fd8f", null ],
    [ "GetAllCinemas", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#acb9dbad7fcf3caacf06a6649d456501f", null ],
    [ "GetCinemaById", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#a88a446886e1dd702e3e708433a990100", null ],
    [ "GetCinemaInStringFormat", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#ac86b213d4dd74d1915f66dadfdb71396", null ],
    [ "GetCinemaWithMostScreeningsOptionsThisWeek", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#ac028212f151323cf8dd79ee8e51bd29a", null ],
    [ "GetMonthlyProfitForAllCinemas", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#a7d1b5c57ad3a3de5869d174e43a331f1", null ],
    [ "UpdateCinema", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html#abd01165bed5f4fdf1e73d8adcba4e214", null ]
];