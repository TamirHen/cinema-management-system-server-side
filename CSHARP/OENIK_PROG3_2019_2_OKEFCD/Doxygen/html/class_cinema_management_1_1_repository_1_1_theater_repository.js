var class_cinema_management_1_1_repository_1_1_theater_repository =
[
    [ "TheaterRepository", "class_cinema_management_1_1_repository_1_1_theater_repository.html#a39b68600ff693215c23c1d0eafb9db4d", null ],
    [ "Create", "class_cinema_management_1_1_repository_1_1_theater_repository.html#af6c3672350d08189012f1ebd7cff68e8", null ],
    [ "Delete", "class_cinema_management_1_1_repository_1_1_theater_repository.html#a7c85d868204bdd34410979423a2e7a50", null ],
    [ "GetAllTheatersInStirngFormat", "class_cinema_management_1_1_repository_1_1_theater_repository.html#a79347c40c760845d2a48055ceeabce5a", null ],
    [ "GetOneById", "class_cinema_management_1_1_repository_1_1_theater_repository.html#a701b2e524662f3464d5fde75d38581b6", null ],
    [ "Update", "class_cinema_management_1_1_repository_1_1_theater_repository.html#a17d67928cbf893fe887c4eaf649446d5", null ]
];