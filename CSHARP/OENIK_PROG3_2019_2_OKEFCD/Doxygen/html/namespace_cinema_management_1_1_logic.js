var namespace_cinema_management_1_1_logic =
[
    [ "Tests", "namespace_cinema_management_1_1_logic_1_1_tests.html", "namespace_cinema_management_1_1_logic_1_1_tests" ],
    [ "CinemaLogic", "class_cinema_management_1_1_logic_1_1_cinema_logic.html", "class_cinema_management_1_1_logic_1_1_cinema_logic" ],
    [ "CinemaMonthlyProfit", "class_cinema_management_1_1_logic_1_1_cinema_monthly_profit.html", "class_cinema_management_1_1_logic_1_1_cinema_monthly_profit" ],
    [ "CinemaNextWeekScreenings", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings.html", "class_cinema_management_1_1_logic_1_1_cinema_next_week_screenings" ],
    [ "DeserializedMovie", "class_cinema_management_1_1_logic_1_1_deserialized_movie.html", "class_cinema_management_1_1_logic_1_1_deserialized_movie" ],
    [ "ICinemaLogic", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic.html", "interface_cinema_management_1_1_logic_1_1_i_cinema_logic" ],
    [ "IMovieLogic", "interface_cinema_management_1_1_logic_1_1_i_movie_logic.html", "interface_cinema_management_1_1_logic_1_1_i_movie_logic" ],
    [ "IScreeningLogic", "interface_cinema_management_1_1_logic_1_1_i_screening_logic.html", "interface_cinema_management_1_1_logic_1_1_i_screening_logic" ],
    [ "ITheaterLogic", "interface_cinema_management_1_1_logic_1_1_i_theater_logic.html", "interface_cinema_management_1_1_logic_1_1_i_theater_logic" ],
    [ "MovieLogic", "class_cinema_management_1_1_logic_1_1_movie_logic.html", "class_cinema_management_1_1_logic_1_1_movie_logic" ],
    [ "ScreeningLogic", "class_cinema_management_1_1_logic_1_1_screening_logic.html", "class_cinema_management_1_1_logic_1_1_screening_logic" ],
    [ "ScreeningsOfMovie", "class_cinema_management_1_1_logic_1_1_screenings_of_movie.html", "class_cinema_management_1_1_logic_1_1_screenings_of_movie" ],
    [ "TheaterLogic", "class_cinema_management_1_1_logic_1_1_theater_logic.html", "class_cinema_management_1_1_logic_1_1_theater_logic" ]
];