var class_cinema_management_1_1_logic_1_1_cinema_logic =
[
    [ "CinemaLogic", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#aa82dc38570710264d4142792af6d67d0", null ],
    [ "CreateCinema", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#a092a5756a95b20594f2a58afbddd5e65", null ],
    [ "DeleteCinema", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#a03f2bda6e999b04d30223d2f4fa233e4", null ],
    [ "GetAllCinemas", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#a0238cc984a43e2239a55f43b1e3fefa4", null ],
    [ "GetCinemaById", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#a2396952d10ced1ba0f7ade91c389330a", null ],
    [ "GetCinemaInStringFormat", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#abfe63f2bcfe309a69c71c2090fbc8c9f", null ],
    [ "GetCinemaWithMostScreeningsOptionsThisWeek", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#ab7ebe5a14b49c2197e16f39e324cc4ea", null ],
    [ "GetMonthlyProfitForAllCinemas", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#af5c55aaf08d471c7f32a5f77daa96e16", null ],
    [ "UpdateCinema", "class_cinema_management_1_1_logic_1_1_cinema_logic.html#a4f821427f0e065a026ec810992a1bca5", null ]
];