var class_cinema_management_1_1_repository_1_1_cinema_repository =
[
    [ "CinemaRepository", "class_cinema_management_1_1_repository_1_1_cinema_repository.html#a31e211b8989773195b20d10efb2a54e0", null ],
    [ "Create", "class_cinema_management_1_1_repository_1_1_cinema_repository.html#ac3aac901756cec026a41bdae62a4f311", null ],
    [ "Delete", "class_cinema_management_1_1_repository_1_1_cinema_repository.html#ab327cbc5d4d585e6ecd7b82ea1cdc06d", null ],
    [ "GetOneById", "class_cinema_management_1_1_repository_1_1_cinema_repository.html#a07275d8ce488b469d745ce781d6de0c3", null ],
    [ "Update", "class_cinema_management_1_1_repository_1_1_cinema_repository.html#a0de832ac38dcf0fc2e8395c070fdd510", null ]
];