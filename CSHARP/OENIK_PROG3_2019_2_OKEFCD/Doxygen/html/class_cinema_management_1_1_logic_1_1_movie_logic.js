var class_cinema_management_1_1_logic_1_1_movie_logic =
[
    [ "MovieLogic", "class_cinema_management_1_1_logic_1_1_movie_logic.html#aab965f9231100fac5247f5ce49cd43f9", null ],
    [ "CreateMovie", "class_cinema_management_1_1_logic_1_1_movie_logic.html#a25c298ff5198cddc7d1fdc02068abe20", null ],
    [ "DeleteMovie", "class_cinema_management_1_1_logic_1_1_movie_logic.html#a703840ff2149d0aeb9df0e2c78a5b35f", null ],
    [ "GetAllMovies", "class_cinema_management_1_1_logic_1_1_movie_logic.html#a32fa5fe700022a7d7b9df0c7bebb9acd", null ],
    [ "GetMovieById", "class_cinema_management_1_1_logic_1_1_movie_logic.html#ab36587380475a89c610460605820d4ca", null ],
    [ "MovieGenerator", "class_cinema_management_1_1_logic_1_1_movie_logic.html#a29d941650bfb389d60a3a5e2c7c87b73", null ],
    [ "UpdateMovie", "class_cinema_management_1_1_logic_1_1_movie_logic.html#a3e26e10304f337abbb6021497b68e5b8", null ]
];