var interface_cinema_management_1_1_repository_1_1_i_screening_repository =
[
    [ "Create", "interface_cinema_management_1_1_repository_1_1_i_screening_repository.html#aadf8e57e0ea21471199425c009250bd5", null ],
    [ "Delete", "interface_cinema_management_1_1_repository_1_1_i_screening_repository.html#a90ac044ed68f6e41accb7c5706e0fc10", null ],
    [ "GetAllScreeningsInStirngFormat", "interface_cinema_management_1_1_repository_1_1_i_screening_repository.html#aaca5e0c5c69a8df9db0d48f92442fbe8", null ],
    [ "GetUpcomingWeekScreeningsInStirngFormat", "interface_cinema_management_1_1_repository_1_1_i_screening_repository.html#a8c67d24541eb7aaf97a19f9388c8b582", null ],
    [ "Update", "interface_cinema_management_1_1_repository_1_1_i_screening_repository.html#a9f128dd738a99df0140d5e50294a868a", null ]
];