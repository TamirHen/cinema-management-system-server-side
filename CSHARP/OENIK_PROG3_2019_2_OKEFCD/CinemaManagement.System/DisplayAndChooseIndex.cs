﻿namespace CinemaManagement.System
{
    using CinemaManagement.Data;
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Data.Entity;
    using global::System.Linq;
    using global::System.Text;
    using global::System.Threading.Tasks;

    /// <summary>
    /// Help Methods to display data on the screen and get results.
    /// </summary>
    public class DisplayAndChooseIndex
    {
        /// <summary>
        /// Method to display all cinemas and get the wanted index of it (index + 1).
        /// </summary>
        /// <param name="cinemaList">List of cinemas.</param>
        /// <returns>index (int) of the wanted cinema from the list.</returns>
        public static int DisplayAndChooseCinemas(IList<Cinema> cinemaList)
        {
            Console.WriteLine("\nPlease choose cinema:\n");
            int i = 1;
            foreach (var cinema in cinemaList)
            {
                Console.WriteLine($"{i}: {cinema.Name}");
                i++;
            }

            Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
            int choice = int.Parse(Console.ReadLine());
            while (choice < 1 || choice > i - 1)
            {
                Console.WriteLine("\n----- Invalid input -----");
                Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
                choice = int.Parse(Console.ReadLine());
            }

            return choice;
        }

        /// <summary>
        /// Method to display all theaters in a single cinema and get the wanted index of it (index + 1).
        /// </summary>
        /// <param name="theaterList">List of theaters.</param>
        /// <returns>index (int) of the wanted theater from the list.</returns>
        public static int DisplayAndChooseTheaters(IList<Theater> theaterList)
        {
            Console.WriteLine("Please choose theater:\n");
            int i = 1;
            foreach (var theater in theaterList)
            {
                Console.WriteLine($"{i}: {theater.GetNameInStringFormat()}");
                i++;
            }

            Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
            int choice = int.Parse(Console.ReadLine());
            while (choice < 1 || choice > i - 1)
            {
                Console.WriteLine("\n----- Invalid input -----");
                Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
                choice = int.Parse(Console.ReadLine());
            }

            return choice;
        }

        /// <summary>
        /// Method to display all movies and get the wanted index of it (index + 1).
        /// </summary>
        /// <param name="movieList">List of movies.</param>
        /// <returns>index (int) of the wanted movie from the list.</returns>
        public static int DisplayAndChooseMovie(IList<Movie> movieList)
        {
            Console.WriteLine("Please choose Movie:\n");
            int i = 1;
            foreach (var movie in movieList)
            {
                Console.WriteLine($"{i}: {movie.Name}");
                i++;
            }

            Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
            int choice = int.Parse(Console.ReadLine());
            while (choice < 1 || choice > i - 1)
            {
                Console.WriteLine("\n----- Invalid input -----");
                Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
                choice = int.Parse(Console.ReadLine());
            }

            return choice;
        }

        /// <summary>
        /// Method to display all screening in a single cinema and get the wanted index of it (index + 1).
        /// </summary>
        /// <param name="screeningList">List of screenings.</param>
        /// <returns>index (int) of the wanted screening from the list.</returns>
        public static int DisplayAndChooseScreening(IList<Screening> screeningList)
        {
            Console.WriteLine("\nPlease choose Screening:\n");
            int i = 1;
            foreach (var screening in screeningList)
            {
                Console.WriteLine($"{i}:\n" +
                    $"  ID: {screening.Id}\n" +
                    $"  Movie: {screening.Movie.Name}\n" +
                    $"  Theater: {screening.Theater.Number}\n" +
                    $"  Screening time: {screening.GetStartTimeInStringFormat()}\n");
                i++;
            }

            Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
            int choice = int.Parse(Console.ReadLine());
            while (choice < 1 || choice > i - 1)
            {
                Console.WriteLine("\n----- Invalid input -----");
                Console.WriteLine($"\nEnter your choice (1-{i - 1}): ");
                choice = int.Parse(Console.ReadLine());
            }

            return choice;
        }
    }
}
