﻿namespace CinemaManagement.System
{
    using CinemaManagement.Data;
    using CinemaManagement.Logic;
    using global::System;
    using global::System.Linq;

    /// <summary>
    /// Class for all the "Update" operations in the console app.
    /// </summary>
    public class UpdateMethods
    {
        /// <summary>
        /// Method for the cinema update in the console app.
        /// </summary>
        /// <param name="cinemaToUpdate">Cinema to update.</param>
        /// <returns>New cinema object with the new wanted values.</returns>
        public static Cinema UpdateCinema(Cinema cinemaToUpdate)
        {
            Console.WriteLine($"\nCurrent name: {cinemaToUpdate.Name}\n");
            Console.WriteLine("Enter new name: ");
            string name = Console.ReadLine();
            Console.WriteLine($"\nCurrent ticket price: {cinemaToUpdate.GetTicketPriceInStringFormat()}\n");
            Console.WriteLine("Enter new ticket price: ");
            string tp = Console.ReadLine();
            decimal ticketPrice;
            decimal.TryParse(tp, out ticketPrice);
            Console.WriteLine($"\nCurrent address: {cinemaToUpdate.Address}\n");
            Console.WriteLine("Enter new address: ");
            string address = Console.ReadLine();
            Console.WriteLine($"\nCurrent open hour: {cinemaToUpdate.GetOpenTimeInStringFormat()}\n");
            Console.WriteLine("Enter new open hour (e.g: 14:30): ");
            string ot = Console.ReadLine();
            DateTime openTime;
            DateTime.TryParse(ot, out openTime);
            Console.WriteLine($"\nCurrent close hour: {cinemaToUpdate.GetCloseTimeInStringFormat()}\n");
            Console.WriteLine("Enter new close hour (e.g: 22:00): ");
            string ct = Console.ReadLine();
            DateTime closeTime;
            DateTime.TryParse(ct, out closeTime);
            return new Cinema
            {
                Id = cinemaToUpdate.Id,
                Name = name,
                TicketPrice = ticketPrice,
                Address = address,
                NumberOfTheaters = cinemaToUpdate.NumberOfTheaters,
                OpenTime = openTime,
                CloseTime = closeTime,
            };
        }

        /// <summary>
        /// Method for the theator update in the console app.
        /// </summary>
        /// <param name="cinemaToUpdate">The cinema this new theater belongs to.</param>
        /// <param name="theaterToUpdate">The theater to update.</param>
        /// <returns>New theater object.</returns>
        public static Theater UpdateTheater(Cinema cinemaToUpdate, Theater theaterToUpdate)
        {
            Console.WriteLine($"\nCurrent theater number: {theaterToUpdate.Number}\n");
            Console.WriteLine("Enter new theater number: ");
            string n = Console.ReadLine();
            int number;
            int.TryParse(n, out number);
            Console.WriteLine($"\nCurrent capacity: {theaterToUpdate.Capacity}\n");
            Console.WriteLine("Enter new capacity: ");
            string c = Console.ReadLine();
            int capacity;
            int.TryParse(c, out capacity);
            Console.WriteLine($"\nCurrent number of rows: {theaterToUpdate.NumberOfRows}\n");
            Console.WriteLine("Enter new number of rows: ");
            string nor = Console.ReadLine();
            int numberOfRows;
            int.TryParse(nor, out numberOfRows);
            Console.WriteLine($"\nCurrent screen size: {theaterToUpdate.GetScreenSizeInStringFormat()}\n");
            Console.WriteLine("Enter new screen size: ");
            string scs = Console.ReadLine();
            int screenSize;
            int.TryParse(scs, out screenSize);
            return new Theater
            {
                Id = theaterToUpdate.Id,
                Number = number,
                Capacity = capacity,
                NumberOfRows = numberOfRows,
                ScreenSize = screenSize,
                CinemaId = cinemaToUpdate.Id,
                Cinema = cinemaToUpdate,
            };
        }

        /// <summary>
        /// Method for the screening update in the console app.
        /// </summary>
        /// <param name="cinemaToUpdate">The cinema this new screening belongs to.</param>
        /// <param name="screeningToUpdate">The screening to update.</param>
        /// <param name="ctx">DB context.</param>
        /// <returns>New screening object.</returns>
        public static Screening UpdateScreening(Cinema cinemaToUpdate, Screening screeningToUpdate, CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            ITheaterLogic theaterLogic = new TheaterLogic(ctx);
            Console.WriteLine($"\nCurrent movie: {screeningToUpdate.Movie.Name}\n");
            int movieIndex = DisplayAndChooseIndex.DisplayAndChooseMovie(movieLogic.GetAllMovies()) - 1;
            Movie movieToUpdate = movieLogic.GetAllMovies().ElementAtOrDefault(movieIndex);
            Console.WriteLine($"\nCurrent theater: {screeningToUpdate.Theater.GetNameInStringFormat()}\n");
            int theaterIndex = DisplayAndChooseIndex.DisplayAndChooseTheaters(cinemaToUpdate.Theaters.OrderBy(x => x.Id).ToList()) - 1;
            Theater theaterToUpdate = theaterLogic.GetAllTheaters().ElementAtOrDefault(theaterIndex);
            Console.WriteLine($"\nCurrent date and hour: {screeningToUpdate.GetStartTimeInStringFormat()}-{screeningToUpdate.GetEndTimeInStringFormat()}\n");
            Console.WriteLine("Enter new date and hour for this screening (e.g: mm/dd/yyyy hh:mm): ");
            string st = Console.ReadLine();
            DateTime startTime;
            DateTime.TryParse(st, out startTime);
            Console.WriteLine($"\nCurrent sold tickets: {screeningToUpdate.TicketsSold}\n");
            Console.WriteLine("Enter new number of sold tickets: ");
            string ts = Console.ReadLine();
            int.TryParse(ts, out int ticketSold);
            return new Screening
            {
                Id = screeningToUpdate.Id,
                CinemaId = cinemaToUpdate.Id,
                Cinema = cinemaToUpdate,
                MovieId = movieToUpdate.Id,
                Movie = movieToUpdate,
                TheaterId = theaterToUpdate.Id,
                Theater = theaterToUpdate,
                StartTime = startTime,
                TicketsSold = ticketSold,
            };
        }

        /// <summary>
        /// Method for the movie update in the console app.
        /// </summary>
        /// <param name="movieToUpdate">Movie to update.</param>
        /// <returns>New movie object.</returns>
        public static Movie UpdateMovie(Movie movieToUpdate)
        {
            Console.WriteLine($"\nCurrent movie: {movieToUpdate.Name}\n");
            Console.WriteLine("Enter new movie name: ");
            string name = Console.ReadLine();
            Console.WriteLine($"\nCurrent length: {movieToUpdate.Length}\n");
            Console.WriteLine("Enter new length: ");
            string l = Console.ReadLine();
            TimeSpan length;
            TimeSpan.TryParse(l, out length);
            return new Movie
            {
                Id = movieToUpdate.Id,
                Name = name,
                Length = length,
            };
        }
    }
}
