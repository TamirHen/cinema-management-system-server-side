﻿namespace CinemaManagement.System
{
    using CinemaManagement.Data;
    using CinemaManagement.Logic;
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Data.Entity;
    using global::System.Linq;
    using global::System.Threading.Tasks;

    /// <summary>
    /// Class for all the method used for edit data in the program.
    /// </summary>
    public class EditMode
    {
        /// <summary>
        /// Method that can add/delete/modify details about cinemas, theaters, movies and screenings.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public static void EnterEditMode(CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            ICinemaLogic cinemaLogic = new CinemaLogic(ctx);
            Console.WriteLine("--------------------- Edit Mode ---------------------");
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Add an object\n" +
                "2 - Delete an object\n" +
                "3 - Modify an object\n" +
                "4 - Edit movie list\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1":
                        AddCinema(cinemaLogic, ctx);
                        break;
                    case "2":
                        DeleteCinema(cinemaLogic, ctx);
                        break;
                    case "3":
                        ModifyCinema(ctx);
                        break;
                    case "4":
                        EditMovieList(ctx);
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Add an object\n" +
                    "2 - Delete an object\n" +
                    "3 - Modify an object\n" +
                    "4 - Edit movie list\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }

            Console.WriteLine("--------------------- Exit Edit Mode ---------------------");
        }

        /// <summary>
        /// Method for adding data.
        /// </summary>
        /// <param name="cinemaLogic">Cinema Logic interface.</param>
        /// <param name="ctx">DB context.</param>
        public static void AddCinema(ICinemaLogic cinemaLogic, CinemaDBContext ctx)
        {
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Add new cinema\n" +
                "2 - Enter existing cinema\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1": // Create new cinema:
                        cinemaLogic.CreateCinema(CreateMethods.CreateCinema());
                        break;
                    case "2": // Choose cinema from the list:
                        int index = DisplayAndChooseIndex.DisplayAndChooseCinemas(cinemaLogic.GetAllCinemas()) - 1;
                        AddObjectToCinema(cinemaLogic.GetAllCinemas().ElementAtOrDefault(index), ctx);
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Add new cinema\n" +
                    "2 - Enter existing cinema\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Method for deleting data.
        /// </summary>
        /// <param name="cinemaLogic">Cinema logic.</param>
        /// <param name="ctx">DB context.</param>
        public static void DeleteCinema(ICinemaLogic cinemaLogic, CinemaDBContext ctx)
        {
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Delete entire cinema\n" +
                "2 - Enter existing cinema\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1": // Delete entire cinema:
                        int index = DisplayAndChooseIndex.DisplayAndChooseCinemas(cinemaLogic.GetAllCinemas()) - 1;
                        cinemaLogic.DeleteCinema(cinemaLogic.GetAllCinemas().ElementAt(index));
                        break;
                    case "2": // Choose cinema form the list:
                        index = DisplayAndChooseIndex.DisplayAndChooseCinemas(cinemaLogic.GetAllCinemas()) - 1;
                        DeleteCinemaObject(cinemaLogic.GetAllCinemas().ElementAtOrDefault(index), ctx);
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Delete entire cinema\n" +
                    "2 - Enter existing cinema\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Method for modifying data.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public static void ModifyCinema(CinemaDBContext ctx)
        {
            ICinemaLogic cinemaLogic = new CinemaLogic(ctx);
            ITheaterLogic theaterLogic = new TheaterLogic(ctx);
            IScreeningLogic screeningLogic = new ScreeningLogic(ctx);
            int index = DisplayAndChooseIndex.DisplayAndChooseCinemas(cinemaLogic.GetAllCinemas()) - 1;
            Console.WriteLine("\n" +
                "What would you like to modify?\n" +
                "1 - Cinema's details\n" +
                "2 - Cinema's Theaters\n" +
                "3 - Cinema's Screenings\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1":
                        cinemaLogic.UpdateCinema(UpdateMethods.UpdateCinema(cinemaLogic.GetAllCinemas().ElementAt(index)));
                        break;
                    case "2":
                        var cinema = cinemaLogic.GetAllCinemas().ElementAt(index);
                        int theaterIndex = DisplayAndChooseIndex.DisplayAndChooseTheaters(cinema.Theaters.OrderBy(x => x.Id).ToList()) - 1;
                        theaterLogic.UpdateTheater(UpdateMethods.UpdateTheater(cinema, cinema.Theaters.ElementAt(theaterIndex)));
                        break;
                    case "3":
                        cinema = cinemaLogic.GetAllCinemas().ElementAt(index);
                        int screeningIndex = DisplayAndChooseIndex.DisplayAndChooseScreening(cinema.Screenings.OrderBy(x => x.Id).ToList()) - 1;
                        screeningLogic.UpdateScreening(UpdateMethods.UpdateScreening(cinema, cinema.Screenings.ElementAt(screeningIndex), ctx));
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to modify?\n" +
                    "1 - Cinema's details\n" +
                    "2 - Cinema's Theaters\n" +
                    "3 - Cinema's Screenings\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Method to add theater or screening to the cinema.
        /// </summary>
        /// <param name="cinema">Cinema to add object to.</param>
        /// <param name="ctx">DB context.</param>
        public static void AddObjectToCinema(Cinema cinema, CinemaDBContext ctx)
        {
            ITheaterLogic theaterLogic = new TheaterLogic(ctx);
            IScreeningLogic screeningLogic = new ScreeningLogic(ctx);
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Add theater\n" +
                "2 - Add screening\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1": // Add theater:
                        theaterLogic.CreateTheater(CreateMethods.CreateTheater(cinema));
                        break;
                    case "2": // Add screening:
                        screeningLogic.CreateScreening(CreateMethods.CreateScreening(cinema, ctx));
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Add theater\n" +
                    "2 - Add screening\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Method to delete theater or screening from the cinema.
        /// </summary>
        /// <param name="cinema">Cinema to delete object from.</param>
        /// <param name="ctx">DB context.</param>
        public static void DeleteCinemaObject(Cinema cinema, CinemaDBContext ctx)
        {
            ITheaterLogic theaterLogic = new TheaterLogic(ctx);
            IScreeningLogic screeningLogic = new ScreeningLogic(ctx);
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Delete theater\n" +
                "2 - Delete screening\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1": // Delete theater:
                        int index = DisplayAndChooseIndex.DisplayAndChooseTheaters(cinema.Theaters.ToList()) - 1;
                        theaterLogic.DeleteTheater(cinema.Theaters.ToList().ElementAt(index));
                        break;
                    case "2": // Delete screening:
                        index = DisplayAndChooseIndex.DisplayAndChooseScreening(cinema.Screenings.ToList()) - 1;
                        screeningLogic.DeleteScreening(cinema.Screenings.ToList().ElementAt(index));
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Delete theater\n" +
                    "2 - Delete screening\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Method to make changes in the movie list.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public static void EditMovieList(CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Add movie\n" +
                "2 - Delete/Modify movie\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1": // Add movie.
                        AddMovieToMovieList(ctx);
                        break;
                    case "2": // Delete/Modify movie.
                        DeleteModifyMovieInMovieList(ctx);
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Add movie\n" +
                    "2 - Delete/Modify movie\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }

        /// <summary>
        /// Method to add movie to the movie list.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public static void AddMovieToMovieList(CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            movieLogic.CreateMovie(CreateMethods.CreateMovie());
        }

        /// <summary>
        /// Method to delete/modify movie from the movie list.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public static void DeleteModifyMovieInMovieList(CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            var movieList = movieLogic.GetAllMovies();
            Console.WriteLine("\nChoose movie to delete/modify:\n");
            int i = 1;
            foreach (var movie in movieList)
            {
                Console.WriteLine($"{i}: {movie.Name}");
                i++;
            }

            Console.WriteLine(); // add empty line after the movie list.
            int indexOfPickedMovie = int.Parse(Console.ReadLine()) - 1;
            var pickedMovie = movieList.AsEnumerable().ElementAtOrDefault(indexOfPickedMovie);
            Console.WriteLine("\nMovie details:\n" +
                $"   ID: {pickedMovie.Id}\n" +
                $"   Name: {pickedMovie.Name}\n" +
                $"   Length: {pickedMovie.GetLengthInStringFormat()}\n" +
                $"   Rank: {pickedMovie.Rank ?? 0}");

            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - Delete this movie\n" +
                "2 - Modify this movie\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            int flag = 0;
            while (input != "0")
            {
                switch (input)
                {
                    case "1": // Delete movie:
                        movieLogic.DeleteMovie(pickedMovie);
                        flag = 1;
                        break;
                    case "2": // Modify movie:
                        movieLogic.UpdateMovie(UpdateMethods.UpdateMovie(pickedMovie));
                        flag = 1;
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        flag = 0;
                        break;
                }

                if (flag == 1)
                {
                    flag = 0;
                    break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Delete this movie\n" +
                    "2 - Modify this movie\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }
        }
    }
}