﻿namespace CinemaManagement.System
{
    using CinemaManagement.Data;
    using CinemaManagement.Logic;
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Data.Entity;

    /// <summary>
    /// Class for viewing all the date.
    /// </summary>
    public class ViewMode
    {
        /// <summary>
        ///  Method that can only display details.
        /// </summary>
        /// <param name="ctx">DB context.</param>
        public static void EnterViewMode(CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            ICinemaLogic cinemaLogic = new CinemaLogic(ctx);
            IScreeningLogic screeningLogic = new ScreeningLogic(ctx);
            Console.WriteLine("--------------------- View Mode ---------------------");
            Console.WriteLine("\n" +
                "What would you like to do?\n\n" +
                "1 - List all movies\n" +
                "2 - List all cinemas\n" +
                "3 - Show all screenings divided by movies\n" +
                "4 - Show the cinema with the most screening options this week\n" +
                "5 - Show Monthly profit for each cinema\n\n" +
                "Press 0 to return.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1":
                        // List all movies.
                        foreach (var movie in movieLogic.GetAllMovies())
                        {
                            Console.WriteLine(movie.ToString());
                        }

                        break;
                    case "2":
                        // List all cinemas.
                        foreach (var cinema in cinemaLogic.GetAllCinemas())
                        {
                            Console.WriteLine(cinemaLogic.GetCinemaInStringFormat(cinema));
                        }

                        break;
                    case "3":
                        foreach (var item in screeningLogic.GetAllScreeningsDividedByMovie())
                        {
                            Console.WriteLine(item.ToString());
                        }

                        break;
                    case "4":
                        Console.WriteLine(cinemaLogic.GetCinemaWithMostScreeningsOptionsThisWeek().ToString());
                        break;
                    case "5":
                        foreach (var item in cinemaLogic.GetMonthlyProfitForAllCinemas())
                        {
                            Console.WriteLine(item.ToString());
                        }

                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - List all movies\n" +
                    "2 - List all cinemas\n" +
                    "3 - Show all screenings divided by movies\n" +
                    "4 - Show the cinema with the most screening options this week\n" +
                    "5 - Show monthly profit for each cinema\n\n" +
                    "Press 0 to return.\n");
                input = Console.ReadLine();
            }

            Console.WriteLine("--------------------- Exit View Mode ---------------------");
        }
    }
}
