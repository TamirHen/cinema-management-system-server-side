﻿namespace CinemaManagement.System
{
    using CinemaManagement.Data;
    using CinemaManagement.Logic;
    using global::System;

    /// <summary>
    /// Class "Program" to run the project in consule application.
    /// </summary>
    public class Program
    {
        private static void Main(string[] args)
        {
            CinemaDBContext ctx = new CinemaDBContext();
            IMovieLogic movieLogic = new MovieLogic(ctx);
            Console.WriteLine("\n--------------------- Main Menu ---------------------");
            Console.WriteLine("\n" +
                "Welcome to the Cinema Management app.\n" +
                "What would you like to do?\n\n" +
                "1 - Enter view mode\n" +
                "2 - Enter edit mode\n" +
                "3 - Generate movies (java endpoint)\n\n" +
                "Press 0 to exit the program.\n");
            string input = Console.ReadLine();
            while (input != "0")
            {
                switch (input)
                {
                    case "1":
                        ViewMode.EnterViewMode(ctx);
                        break;
                    case "2":
                        EditMode.EnterEditMode(ctx);
                        break;
                    case "3":
                        Console.WriteLine(movieLogic.MovieGenerator());
                        break;
                    default:
                        Console.WriteLine("\n----- Invalid input -----");
                        break;
                }

                Console.WriteLine("\n--------------------- Main Menu ---------------------");
                Console.WriteLine("\n" +
                    "What would you like to do?\n\n" +
                    "1 - Enter view mode\n" +
                    "2 - Enter edit mode\n" +
                    "3 - Generate movies (java endpoint)\n\n" +
                    "Press 0 to exit the program.\n");
                Console.WriteLine("--------------------- Main Menu ---------------------");
                input = Console.ReadLine();
            }

            Console.WriteLine("--------------------- End Main Menu ---------------------");
        }
    }
}
