﻿namespace CinemaManagement.System
{
    using CinemaManagement.Data;
    using CinemaManagement.Logic;
    using global::System;
    using global::System.Linq;

    /// <summary>
    /// Class for all the "Create" operations in the console app.
    /// </summary>
    public class CreateMethods
    {
        /// <summary>
        /// Method for the cinema creation in the console app.
        /// </summary>
        /// <returns>Cinema to create.</returns>
        public static Cinema CreateCinema()
        {
            Console.WriteLine("Enter cinema name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter ticket price: ");
            string tp = Console.ReadLine();
            decimal ticketPrice;
            decimal.TryParse(tp, out ticketPrice);
            Console.WriteLine("Enter address: ");
            string address = Console.ReadLine();
            Console.WriteLine("Enter open hour (e.g: 14:30): ");
            string ot = Console.ReadLine();
            DateTime openTime;
            DateTime.TryParse(ot, out openTime);
            Console.WriteLine("Enter close hour (e.g: 22:00): ");
            string ct = Console.ReadLine();
            DateTime closeTime;
            DateTime.TryParse(ct, out closeTime);
            return new Cinema
            {
                Name = name,
                TicketPrice = ticketPrice,
                Address = address,
                NumberOfTheaters = 0,
                OpenTime = openTime,
                CloseTime = closeTime,
            };
        }

        /// <summary>
        /// Method for the theator creation in the console app.
        /// </summary>
        /// <param name="cinemaToAddTheaterTo">The cinema this new theater belongs to.</param>
        /// <returns>New theater object.</returns>
        public static Theater CreateTheater(Cinema cinemaToAddTheaterTo)
        {
            Console.WriteLine("Enter theater number: ");
            string n = Console.ReadLine();
            int number;
            int.TryParse(n, out number);
            Console.WriteLine("Enter capacity: ");
            string c = Console.ReadLine();
            int capacity;
            int.TryParse(c, out capacity);
            Console.WriteLine("Enter number of rows: ");
            string nor = Console.ReadLine();
            int numberOfRows;
            int.TryParse(nor, out numberOfRows);
            Console.WriteLine("Enter screen size: ");
            string scs = Console.ReadLine();
            int screenSize;
            int.TryParse(scs, out screenSize);
            return new Theater
            {
                Number = number,
                Capacity = capacity,
                NumberOfRows = numberOfRows,
                ScreenSize = screenSize,
                CinemaId = cinemaToAddTheaterTo.Id,
                Cinema = cinemaToAddTheaterTo,
            };
        }

        /// <summary>
        /// Method for the screening creation in the console app.
        /// </summary>
        /// <param name="cinemaToAddScreeningTo">The cinema this new screening belongs to.</param>
        /// <param name="ctx">DB context.</param>
        /// <returns>New screening object.</returns>
        public static Screening CreateScreening(Cinema cinemaToAddScreeningTo, CinemaDBContext ctx)
        {
            IMovieLogic movieLogic = new MovieLogic(ctx);
            ITheaterLogic theaterLogic = new TheaterLogic(ctx);
            int movieIndex = DisplayAndChooseIndex.DisplayAndChooseMovie(movieLogic.GetAllMovies()) - 1;
            Movie movieToCreate = movieLogic.GetAllMovies().ElementAtOrDefault(movieIndex);
            int theaterIndex = DisplayAndChooseIndex.DisplayAndChooseTheaters(cinemaToAddScreeningTo.Theaters.ToList()) - 1;
            Theater theaterToCreate = theaterLogic.GetAllTheaters().ElementAtOrDefault(theaterIndex);
            Console.WriteLine("Enter date and hour for this screening (e.g: mm/dd/yyyy hh:mm): ");
            string st = Console.ReadLine();
            DateTime startTime;
            DateTime.TryParse(st, out startTime);
            return new Screening
            {
                CinemaId = cinemaToAddScreeningTo.Id,
                Cinema = cinemaToAddScreeningTo,
                MovieId = movieToCreate.Id,
                Movie = movieToCreate,
                TheaterId = theaterToCreate.Id,
                Theater = theaterToCreate,
                StartTime = startTime,
                TicketsSold = 0,
            };
        }

        /// <summary>
        /// Method for the movie creation in the console app.
        /// </summary>
        /// <returns>Movie to create.</returns>
        public static Movie CreateMovie()
        {
            Console.WriteLine("Enter movie name: ");
            string name = Console.ReadLine();
            Console.WriteLine("Enter length: ");
            string l = Console.ReadLine();
            TimeSpan length;
            TimeSpan.TryParse(l, out length);
            return new Movie
            {
                Name = name,
                Rank = null,
                Length = length,
            };
        }
    }
}
