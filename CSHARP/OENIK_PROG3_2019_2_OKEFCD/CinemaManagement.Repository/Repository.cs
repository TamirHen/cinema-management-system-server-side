﻿namespace CinemaManagement.Repository
{
    using System;
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Repository abstract class for GetAll and GetOneById methods.
    /// </summary>
    /// <typeparam name="T">Class type.</typeparam>
    public abstract class Repository<T> : IDisposable, IRepository<T>
        where T : class
    {
        /// <summary>
        /// Is disposed (true/false).
        /// </summary>
        private bool disposed = false;

                /// <summary>
        /// Initializes a new instance of the <see cref="Repository{T}"/> class.
        /// </summary>
        /// <param name="ctx">DB Context.</param>
        public Repository(CinemaDBContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets the DB context.
        /// </summary>
        public CinemaDBContext Ctx { get; set; }

        /// <summary>
        /// Get all T elements.
        /// </summary>
        /// <returns>IQueryable of T elements.</returns>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }

        /// <summary>
        /// Abstract method to get element by it's id.
        /// </summary>
        /// <param name="id">Id of wanted element.</param>
        /// <returns>Wanted element of type T.</returns>
        public abstract T GetOneById(int id);

        /// <inheritdoc/>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Method to dispose the context.
        /// </summary>
        /// <param name="disposing">Dispose or not (true/false).</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.Ctx.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}
