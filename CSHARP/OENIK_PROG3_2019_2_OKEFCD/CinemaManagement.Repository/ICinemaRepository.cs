﻿namespace CinemaManagement.Repository
{
    using CinemaManagement.Data;

    /// <summary>
    /// Repository inteface for cinemas.
    /// </summary>
    public interface ICinemaRepository : IRepository<Cinema>
    {
        /// <summary>
        /// Create new cinema.
        /// </summary>
        /// <param name="cinema">Cinema to create.</param>
        void Create(Cinema cinema);

        /// <summary>
        /// Delete cinema.
        /// </summary>
        /// <param name="cinema">Cinema to delete.</param>
        void Delete(Cinema cinema);

        /// <summary>
        /// Update cinema.
        /// </summary>
        /// <param name="cinema">Cinema to update.</param>
        void Update(Cinema cinema);
    }
}
