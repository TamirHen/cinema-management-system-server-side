﻿namespace CinemaManagement.Repository
{
    using CinemaManagement.Data;

    /// <summary>
    /// Repository inteface for theaters.
    /// </summary>
    public interface ITheaterRepository : IRepository<Theater>
    {
        /// <summary>
        /// Create new theater.
        /// </summary>
        /// <param name="theater">theater to create.</param>
        void Create(Theater theater);

        /// <summary>
        /// Delete theater.
        /// </summary>
        /// <param name="theater">Theater to delete.</param>
        void Delete(Theater theater);

        /// <summary>
        /// Update theater.
        /// </summary>
        /// <param name="theater">theater to update.</param>
        void Update(Theater theater);

        /// <summary>
        /// All theaters of wanted cinema in string format.
        /// </summary>
        /// <param name="cinema">Wanted cinema.</param>
        /// <returns>String of all the theaters in the cinema.</returns>
        string GetAllTheatersInStirngFormat(Cinema cinema);
    }
}
