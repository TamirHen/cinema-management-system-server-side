﻿namespace CinemaManagement.Repository
{
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Repository class for Cinemas.
    /// </summary>
    public class CinemaRepository : Repository<Cinema>, ICinemaRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CinemaRepository"/> class.
        /// </summary>
        /// <param name="ctx">DB Context.</param>
        public CinemaRepository(CinemaDBContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public override Cinema GetOneById(int id)
        {
            return this.GetAll().SingleOrDefault(cinema => cinema.Id == id);
        }

        /// <inheritdoc/>
        public void Create(Cinema cinemaToCreate)
        {
            var cinemas = this.Ctx.Cinemas;
            var listOfIds = from cinema in cinemas
                                   orderby cinema.Id descending
                                   select cinema.Id;
            int nextId = listOfIds.First() + 1;
            cinemaToCreate.Id = nextId;
            this.Ctx.Cinemas.Add(cinemaToCreate);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(Cinema cinema)
        {
            var cinemaToDelete = this.Ctx.Cinemas.Single(x => x.Id == cinema.Id);
            var theatersToDelete = from theater in this.Ctx.Theaters
                                   where theater.CinemaId == cinema.Id
                                   select theater;
            var screeningsToDelete = from screening in this.Ctx.Screenings
                                     where screening.CinemaId == cinema.Id
                                     select screening;

            foreach (var screening in screeningsToDelete)
            {
                this.Ctx.Screenings.Remove(screening);
            }

            foreach (var theater in theatersToDelete)
            {
                this.Ctx.Theaters.Remove(theater);
            }

            this.Ctx.Cinemas.Remove(cinemaToDelete);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Cinema cinema)
        {
            var cinemaToUpdate = this.Ctx.Cinemas.Single(x => x.Id == cinema.Id);
            cinemaToUpdate.Name = cinema.Name;
            cinemaToUpdate.Address = cinema.Address;
            cinemaToUpdate.NumberOfTheaters = cinema.NumberOfTheaters;
            cinemaToUpdate.OpenTime = cinema.OpenTime;
            cinemaToUpdate.CloseTime = cinema.CloseTime;
            cinemaToUpdate.Screenings = cinema.Screenings;
            cinemaToUpdate.Theaters = cinema.Theaters;
            cinemaToUpdate.TicketPrice = cinema.TicketPrice;
            this.Ctx.SaveChanges();
        }
    }
}
