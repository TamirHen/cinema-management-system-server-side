﻿namespace CinemaManagement.Repository
{
    using CinemaManagement.Data;

    /// <summary>
    /// Repository inteface for movies.
    /// </summary>
    public interface IMovieRepository : IRepository<Movie>
    {
        /// <summary>
        /// Create new Movie.
        /// </summary>
        /// <param name="movie">Movie to create.</param>
        void Create(Movie movie);

        /// <summary>
        /// Delete movie.
        /// </summary>
        /// <param name="movie">Movie to delete.</param>
        void Delete(Movie movie);

        /// <summary>
        /// Update movie.
        /// </summary>
        /// <param name="movie">Movie to update.</param>
        void Update(Movie movie);
    }
}
