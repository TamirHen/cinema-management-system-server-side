﻿namespace CinemaManagement.Repository
{
    using System;
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Repository class for Screenings.
    /// </summary>
    public class ScreeningRepository : Repository<Screening>, IScreeningRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScreeningRepository"/> class.
        /// </summary>
        /// <param name="ctx">DB Context.</param>
        public ScreeningRepository(CinemaDBContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public override Screening GetOneById(int id)
        {
            return this.GetAll().SingleOrDefault(screening => screening.Id == id);
        }

        /// <inheritdoc/>
        public void Create(Screening screeningToCreate)
        {
            var screenings = this.Ctx.Screenings;
            var listOfIds = from screening in screenings
                            orderby screening.Id descending
                            select screening.Id;
            int nextId = listOfIds.First() + 1;
            screeningToCreate.Id = nextId;
            this.Ctx.Screenings.Add(screeningToCreate);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(Screening screening)
        {
            var screeningToDelete = this.Ctx.Screenings.Single(x => x.Id == screening.Id);
            this.Ctx.Screenings.Remove(screeningToDelete);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Screening screening)
        {
            var screeningToUpdate = this.Ctx.Screenings.Single(x => x.Id == screening.Id);
            screeningToUpdate.Cinema = screening.Cinema;
            screeningToUpdate.CinemaId = screening.CinemaId;
            screeningToUpdate.Movie = screening.Movie;
            screeningToUpdate.MovieId = screening.MovieId;
            screeningToUpdate.StartTime = screening.StartTime;
            screeningToUpdate.Theater = screening.Theater;
            screeningToUpdate.TheaterId = screening.TheaterId;
            screeningToUpdate.TicketsSold = screening.TicketsSold;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public string GetAllScreeningsInStirngFormat(Cinema cinema)
        {
            var screenings = cinema.Screenings.OrderBy(x => x.Id);
            string screeningsDetails = string.Empty;
            foreach (var screening in screenings)
            {
                screeningsDetails += $"{screening.ToString()}\n";
            }

            return screeningsDetails;
        }

        /// <inheritdoc/>
        public string GetUpcomingWeekScreeningsInStirngFormat(Cinema cinema)
        {
            var screenings = cinema.Screenings.OrderBy(x => x.Id);
            string screeningsDetails = string.Empty;
            var screeningEndTime = DateTime.Now.AddDays(7);
            foreach (var screening in screenings)
            {
                if (screening.StartTime > DateTime.Now && screening.StartTime < screeningEndTime)
                {
                    screeningsDetails += $"{screening.ToString()}\n";
                }
            }

            return screeningsDetails;
        }
    }
}
