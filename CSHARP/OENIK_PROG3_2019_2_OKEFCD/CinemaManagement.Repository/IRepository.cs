﻿namespace CinemaManagement.Repository
{
    using System.Linq;

    /// <summary>
    /// Repository interface for all CRUD methods.
    /// </summary>
    /// <typeparam name="T">Class type.</param>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Get one element by it's id.
        /// </summary>
        /// <param name="id">Element's id.</param>
        /// <returns>Wanted element.</returns>
        T GetOneById(int id);

        /// <summary>
        /// Get all elements in the IQueryable.
        /// </summary>
        /// <returns>IQueryable of elements.</returns>
        IQueryable<T> GetAll();
    }
}
