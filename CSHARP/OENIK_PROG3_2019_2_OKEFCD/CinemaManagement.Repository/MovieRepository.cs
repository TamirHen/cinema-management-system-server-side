﻿namespace CinemaManagement.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Repository class for movies.
    /// </summary>
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MovieRepository"/> class.
        /// </summary>
        /// <param name="ctx">DB Context.</param>
        public MovieRepository(CinemaDBContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public override Movie GetOneById(int id)
        {
            return this.GetAll().SingleOrDefault(movie => movie.Id == id);
        }

        /// <inheritdoc/>
        public void Create(Movie movieToCreate)
        {
            var movies = this.Ctx.Movies;
            var listOfIds = from movie in movies
                            orderby movie.Id descending
                            select movie.Id;
            int nextId = listOfIds.First() + 1;
            movieToCreate.Id = nextId;
            this.Ctx.Movies.Add(movieToCreate);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(Movie movie)
        {
            var movieToDelete = this.Ctx.Movies.Single(x => x.Id == movie.Id);
            var screeningsToDelete = from screening in this.Ctx.Screenings
                                     where screening.MovieId == movie.Id
                                     select screening;
            foreach (var screening in screeningsToDelete)
            {
                this.Ctx.Screenings.Remove(screening);
            }

            this.Ctx.Movies.Remove(movieToDelete);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Movie movie)
        {
            var movieToUpdate = this.Ctx.Movies.Single(x => x.Id == movie.Id);
            movieToUpdate.Length = movie.Length;
            movieToUpdate.Name = movie.Name;
            this.Ctx.SaveChanges();
        }
    }
}
