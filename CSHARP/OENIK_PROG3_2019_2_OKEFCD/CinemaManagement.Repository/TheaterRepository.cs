﻿namespace CinemaManagement.Repository
{
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Repository class for Theaters.
    /// </summary>
    public class TheaterRepository : Repository<Theater>, ITheaterRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TheaterRepository"/> class.
        /// </summary>
        /// <param name="ctx">DB Context.</param>
        public TheaterRepository(CinemaDBContext ctx)
            : base(ctx)
        {
        }

        /// <inheritdoc/>
        public override Theater GetOneById(int id)
        {
            return this.GetAll().SingleOrDefault(theater => theater.Id == id);
        }

        /// <inheritdoc/>
        public void Create(Theater theaterToCreate)
        {
            var theaters = this.Ctx.Theaters;
            var listOfIds = from theater in theaters
                            orderby theater.Id descending
                            select theater.Id;
            int nextId = listOfIds.First() + 1;
            theaterToCreate.Id = nextId;
            this.Ctx.Theaters.Add(theaterToCreate);
            var cinemaToIncreaseTheaterNumbers = this.Ctx.Cinemas.SingleOrDefault(x => x.Id == theaterToCreate.CinemaId);
            cinemaToIncreaseTheaterNumbers.NumberOfTheaters++;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Delete(Theater theater)
        {
            var cinemaToDecreaseTheaterNumbers = this.Ctx.Cinemas.SingleOrDefault(x => x.Id == theater.CinemaId);
            cinemaToDecreaseTheaterNumbers.NumberOfTheaters--;
            var theaterToDelete = this.Ctx.Theaters.Single(x => x.Id == theater.Id);
            var screeningsToDelete = from screening in this.Ctx.Screenings
                                     where screening.TheaterId == theater.Id
                                     select screening;
            foreach (var screening in screeningsToDelete)
            {
                this.Ctx.Screenings.Remove(screening);
            }

            this.Ctx.Theaters.Remove(theaterToDelete);
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public void Update(Theater theater)
        {
            var theaterToUpdate = this.Ctx.Theaters.Single(x => x.Id == theater.Id);
            theaterToUpdate.Number = theater.Number;
            theaterToUpdate.Capacity = theater.Capacity;
            theaterToUpdate.NumberOfRows = theater.NumberOfRows;
            theaterToUpdate.Screenings = theater.Screenings;
            theaterToUpdate.ScreenSize = theater.ScreenSize;
            theaterToUpdate.CinemaId = theater.CinemaId;
            theaterToUpdate.Cinema = theater.Cinema;
            this.Ctx.SaveChanges();
        }

        /// <inheritdoc/>
        public string GetAllTheatersInStirngFormat(Cinema cinema)
        {
            var theaters = cinema.Theaters.OrderBy(x => x.Id);
            string theatersDetails = string.Empty;
            foreach (var theater in theaters)
            {
                theatersDetails += $"{theater.ToString()}\n";
            }

            return theatersDetails;
        }
    }
}
