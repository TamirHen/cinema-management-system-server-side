﻿namespace CinemaManagement.Repository
{
    using System.Linq;
    using CinemaManagement.Data;

    /// <summary>
    /// Repository inteface for screenings.
    /// </summary>
    public interface IScreeningRepository : IRepository<Screening>
    {
        /// <summary>
        /// Create new screening.
        /// </summary>
        /// <param name="screening">screening to create.</param>
        void Create(Screening screening);

        /// <summary>
        /// Delete screening.
        /// </summary>
        /// <param name="screening">Screening to delete.</param>
        void Delete(Screening screening);

        /// <summary>
        /// Update screening.
        /// </summary>
        /// <param name="screening">screening to update.</param>
        void Update(Screening screening);

        /// <summary>
        /// All screenings of wanted cinema in string format.
        /// </summary>
        /// <param name="cinema">Wanted cinema.</param>
        /// <returns>String of all the screenings in the cinema.</returns>
        string GetAllScreeningsInStirngFormat(Cinema cinema);

        /// <summary>
        /// All upcoming this week screenings of wanted cinema in string format.
        /// </summary>
        /// <param name="cinema">Wanted cinema.</param>
        /// <returns>String of this week upcoming screenings in the cinema.</returns>
        string GetUpcomingWeekScreeningsInStirngFormat(Cinema cinema);
    }
}
