﻿namespace CinemaManagement.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for all the ScreeningLogic tests.
    /// </summary>
    [TestFixture]
    public class ScreeningTests
    {
        // CRUD Tests:

        /// <summary>
        /// Test that when calling GetScreeningById it exectue once.
        /// </summary>
        /// <param name="id">Id of wanted screening.</param>
        [Test]
        [TestCase(1)]
        [TestCase(-1)]
        public void GetScreeningByIdShouldBeCalledOnce(int id)
        {
            Mock<IScreeningRepository> mockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            IScreeningLogic screeningLogic = new ScreeningLogic(null, mockedRepo.Object);
            var screeningList = screeningLogic.GetScreeningById(id);
            mockedRepo.Verify(repo => repo.GetOneById(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test that when calling GetAllScreenings it exectue once.
        /// </summary>
        [Test]
        public void GetAllScreeningsShouldBeCalledOnce()
        {
            Mock<IScreeningRepository> mockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            IScreeningLogic screeningLogic = new ScreeningLogic(null, mockedRepo.Object);
            var screeningList = screeningLogic.GetAllScreenings();
            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test that when calling CreateScreening it execute once.
        /// </summary>
        [Test]
        public void CreateScreeningShouldBeCalledOnce()
        {
            Mock<IScreeningRepository> mockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            IScreeningLogic screeningLogic = new ScreeningLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:30", out TimeSpan length);
            Screening screening = new Screening
            {
                TicketsSold = 100,
            };
            screeningLogic.CreateScreening(screening);
            mockedRepo.Verify(repo => repo.Create(screening), Times.Once);
        }

        /// <summary>
        /// Test that when calling DeleteScreening it execute once.
        /// </summary>
        [Test]
        public void DeleteScreeningShouldBeCalledOnce()
        {
            Mock<IScreeningRepository> mockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            IScreeningLogic screeningLogic = new ScreeningLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("01:55", out TimeSpan length);
            Screening screening = new Screening()
            {
                TicketsSold = 85,
            };
            screeningLogic.DeleteScreening(screening);
            mockedRepo.Verify(repo => repo.Delete(screening), Times.Once);
        }

        /// <summary>
        /// Test that when calling UpdateScreening it exectue once.
        /// </summary>
        [Test]
        public void UpdateScreeningShouldBeCalledOnce()
        {
            Mock<IScreeningRepository> mockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            IScreeningLogic screeningLogic = new ScreeningLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:25", out TimeSpan length);
            Screening screening = new Screening()
            {
                TicketsSold = 77,
            };
            screeningLogic.UpdateScreening(screening);
            mockedRepo.Verify(repo => repo.Update(screening), Times.Once);
        }

        // Non-CRUD Tests:

        /// <summary>
        /// Test that GetAllScreeningsDividedByMovie returns ScreeningsOfMovie objects in IEnumerable.
        /// </summary>
        [Test]
        public void GetAllScreeningsDividedByMovieShouldReturnIEnumerableScreeningsOfMovieObject()
        {
            Mock<IScreeningRepository> screeningMockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            Mock<IMovieRepository> movieMockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            List<Movie> movies = new List<Movie>
            {
                new Movie
                {
                    Id = 1,
                    Name = "Avengers: Endgame",
                    Length = new TimeSpan(3, 2, 0),
                    Rank = 8.9,
                },
                new Movie
                {
                    Id = 2,
                    Name = "Avengers: Infinity War",
                    Length = new TimeSpan(2, 55, 0),
                    Rank = 8.5,
                },
                new Movie
                {
                    Id = 3,
                    Name = "Avengers: Age of Ultron",
                    Length = new TimeSpan(2, 42, 0),
                    Rank = 7.6,
                },
                new Movie
                {
                    Id = 4,
                    Name = "The Avengers",
                    Length = new TimeSpan(2, 47, 0),
                    Rank = 8,
                },
            };
            Cinema cinema = new Cinema { Name = "Cinema City", TicketPrice = 10, };
            List<Screening> screenings = new List<Screening>
            {
                new Screening
                {
                    Id = 1,
                    TheaterId = 1,
                    MovieId = 2,
                    Movie = movies.ElementAt(1),
                    StartTime = DateTime.Now.AddHours(4),
                    TicketsSold = 65,
                    CinemaId = 1,
                    Cinema = cinema,
                },
                new Screening
                {
                    Id = 2,
                    TheaterId = 1,
                    MovieId = 1,
                    Movie = movies.ElementAt(0),
                    StartTime = DateTime.Now.AddDays(2),
                    TicketsSold = 22,
                    CinemaId = 1,
                    Cinema = cinema,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 1,
                    MovieId = 4,
                    Movie = movies.ElementAt(3),
                    StartTime = DateTime.Now.AddHours(0.5),
                    TicketsSold = 70,
                    CinemaId = 1,
                    Cinema = cinema,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 1,
                    MovieId = 1,
                    Movie = movies.ElementAt(0),
                    StartTime = DateTime.Now.AddHours(0.5),
                    TicketsSold = 70,
                    CinemaId = 1,
                    Cinema = cinema,
                },
            };
            screeningMockedRepo.Setup(x => x.GetAll()).Returns(screenings.AsQueryable);
            movieMockedRepo.Setup(x => x.GetAll()).Returns(movies.AsQueryable);
            List<ScreeningsOfMovie> expectedResult = new List<ScreeningsOfMovie>()
            {
                new ScreeningsOfMovie
                {
                    Movie = movies.ElementAt(0),
                    Screenings = screenings.Where(x => x.MovieId == 1).ToList(),
                },
                new ScreeningsOfMovie
                {
                    Movie = movies.ElementAt(1),
                    Screenings = screenings.Where(x => x.MovieId == 2).ToList(),
                },
                new ScreeningsOfMovie
                {
                    Movie = movies.ElementAt(2),
                    Screenings = screenings.Where(x => x.MovieId == 3).ToList(),
                },
                new ScreeningsOfMovie
                {
                    Movie = movies.ElementAt(3),
                    Screenings = screenings.Where(x => x.MovieId == 4).ToList(),
                },
            };

            ScreeningLogic screeningLogic = new ScreeningLogic(null, screeningMockedRepo.Object, movieMockedRepo.Object);
            var result = screeningLogic.GetAllScreeningsDividedByMovie().ToList();
            Assert.That(result, Is.EquivalentTo(expectedResult));
        }

        /// <summary>
        /// Test that GetAllScreeningsDividedByMovieShould return the correct number of screenings for movie.
        /// </summary>
        [Test]
        public void GetAllScreeningsDividedByMovieShouldReturnTheCorrentNumberOfThisMovieScreenings()
        {
            Mock<IScreeningRepository> screeningMockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            Mock<IMovieRepository> movieMockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            List<Movie> movies = new List<Movie>
            {
                new Movie
                {
                    Id = 1,
                    Name = "Avengers: Endgame",
                    Length = new TimeSpan(3, 2, 0),
                    Rank = 8.9,
                },
                new Movie
                {
                    Id = 2,
                    Name = "Avengers: Infinity War",
                    Length = new TimeSpan(2, 55, 0),
                    Rank = 8.5,
                },
                new Movie
                {
                    Id = 3,
                    Name = "Avengers: Age of Ultron",
                    Length = new TimeSpan(2, 42, 0),
                    Rank = 7.6,
                },
                new Movie
                {
                    Id = 4,
                    Name = "The Avengers",
                    Length = new TimeSpan(2, 47, 0),
                    Rank = 8,
                },
            };
            Cinema cinema = new Cinema { Name = "Cinema City", TicketPrice = 10, };
            List<Screening> screenings = new List<Screening>
            {
                new Screening
                {
                    Id = 1,
                    TheaterId = 1,
                    MovieId = 2,
                    Movie = movies.ElementAt(1),
                    StartTime = DateTime.Now.AddHours(4),
                    TicketsSold = 65,
                    CinemaId = 1,
                    Cinema = cinema,
                },
                new Screening
                {
                    Id = 2,
                    TheaterId = 1,
                    MovieId = 1,
                    Movie = movies.ElementAt(0),
                    StartTime = DateTime.Now.AddDays(2),
                    TicketsSold = 22,
                    CinemaId = 1,
                    Cinema = cinema,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 1,
                    MovieId = 4,
                    Movie = movies.ElementAt(3),
                    StartTime = DateTime.Now.AddHours(0.5),
                    TicketsSold = 70,
                    CinemaId = 1,
                    Cinema = cinema,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 1,
                    MovieId = 1,
                    Movie = movies.ElementAt(0),
                    StartTime = DateTime.Now.AddHours(0.5),
                    TicketsSold = 70,
                    CinemaId = 1,
                    Cinema = cinema,
                },
            };
            screeningMockedRepo.Setup(x => x.GetAll()).Returns(screenings.AsQueryable);
            movieMockedRepo.Setup(x => x.GetAll()).Returns(movies.AsQueryable);

            ScreeningLogic screeningLogic = new ScreeningLogic(null, screeningMockedRepo.Object, movieMockedRepo.Object);
            var result = screeningLogic.GetAllScreeningsDividedByMovie();
            Assert.That(result.Where(x => x.Movie.Id == 1).FirstOrDefault().Screenings.Count(), Is.EqualTo(2));
            Assert.That(result.Where(x => x.Movie.Id == 3).FirstOrDefault().Screenings.Count(), Is.EqualTo(0));
        }
    }
}
