﻿namespace CinemaManagement.Logic.Tests
{
    using System;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for all the MovieLogic tests.
    /// </summary>
    [TestFixture]
    public class MovieTests
    {
        /// <summary>
        /// Test that when calling GetMovieById it exectue once.
        /// </summary>
        /// <param name="id">Id of wanted movie.</param>
        [Test]
        [TestCase(1)]
        [TestCase(-1)]
        public void GetMovieByIdShouldBeCalledOnce(int id)
        {
            Mock<IMovieRepository> mockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            IMovieLogic movieLogic = new MovieLogic(null, mockedRepo.Object);
            var movieList = movieLogic.GetMovieById(id);
            mockedRepo.Verify(repo => repo.GetOneById(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test that when calling GetAllMovies it exectue once.
        /// </summary>
        [Test]
        public void GetAllMoviesShouldBeCalledOnce()
        {
            Mock<IMovieRepository> mockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            IMovieLogic movieLogic = new MovieLogic(null, mockedRepo.Object);
            var movieList = movieLogic.GetAllMovies();
            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test that when calling CreateMovie it execute once.
        /// </summary>
        [Test]
        public void CreateMovieShouldBeCalledOnce()
        {
            Mock<IMovieRepository> mockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            IMovieLogic movieLogic = new MovieLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:30", out TimeSpan length);
            Movie movie = new Movie
            {
                Name = "Test Name",
                Rank = 9.3,
                Length = length,
            };
            movieLogic.CreateMovie(movie);
            mockedRepo.Verify(repo => repo.Create(movie), Times.Once);
        }

        /// <summary>
        /// Test that when calling DeleteMovie it execute once.
        /// </summary>
        [Test]
        public void DeleteMovieShouldBeCalledOnce()
        {
            Mock<IMovieRepository> mockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            IMovieLogic movieLogic = new MovieLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("01:55", out TimeSpan length);
            Movie movie = new Movie()
            {
                Name = "TestName",
                Rank = 8.1,
                Length = length,
            };
            movieLogic.DeleteMovie(movie);
            mockedRepo.Verify(repo => repo.Delete(movie), Times.Once);
        }

        /// <summary>
        /// Test that when calling UpdateMovie it exectue once.
        /// </summary>
        [Test]
        public void UpdateMovieShouldBeCalledOnce()
        {
            Mock<IMovieRepository> mockedRepo = new Mock<IMovieRepository>(MockBehavior.Loose);
            IMovieLogic movieLogic = new MovieLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:25", out TimeSpan length);
            Movie movie = new Movie()
            {
                Name = "TestName",
                Rank = 8.1,
                Length = length,
            };
            movieLogic.UpdateMovie(movie);
            mockedRepo.Verify(repo => repo.Update(movie), Times.Once);
        }
    }
}
