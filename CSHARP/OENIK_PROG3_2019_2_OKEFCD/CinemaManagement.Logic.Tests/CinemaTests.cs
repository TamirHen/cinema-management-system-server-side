﻿namespace CinemaManagement.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for all the CinemaLogic tests.
    /// </summary>
    [TestFixture]
    public class CinemaTests
    {
        // CRUD Tests:

        /// <summary>
        /// Test that when calling GetCinemaById it exectue once.
        /// </summary>
        /// <param name="id">Id of wanted cinema.</param>
        [Test]
        [TestCase(1)]
        [TestCase(-1)]
        public void GetCinemaByIdShouldBeCalledOnce(int id)
        {
            Mock<ICinemaRepository> mockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, mockedRepo.Object);
            var cinemaList = cinemaLogic.GetCinemaById(id);
            mockedRepo.Verify(repo => repo.GetOneById(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test that when calling GetAllCinemas it exectue once.
        /// </summary>
        [Test]
        public void GetAllCinemasShouldBeCalledOnce()
        {
            Mock<ICinemaRepository> mockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, mockedRepo.Object);
            var cinemaList = cinemaLogic.GetAllCinemas();
            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test that when calling CreateCinema it execute once.
        /// </summary>
        [Test]
        public void CreateCinemaShouldBeCalledOnce()
        {
            Mock<ICinemaRepository> mockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:30", out TimeSpan length);
            Cinema cinema = new Cinema
            {
                Name = "Test Cinema",
                Address = "Budapest, some street 5",
                NumberOfTheaters = 0,
                TicketPrice = 9,
            };
            cinemaLogic.CreateCinema(cinema);
            mockedRepo.Verify(repo => repo.Create(cinema), Times.Once);
        }

        /// <summary>
        /// Test that when calling DeleteCinema it execute once.
        /// </summary>
        [Test]
        public void DeleteCinemaShouldBeCalledOnce()
        {
            Mock<ICinemaRepository> mockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("01:55", out TimeSpan length);
            Cinema cinema = new Cinema()
            {
                Name = "Test Cinema",
                Address = "Budapest, some street 2",
                NumberOfTheaters = 0,
                TicketPrice = 7,
            };
            cinemaLogic.DeleteCinema(cinema);
            mockedRepo.Verify(repo => repo.Delete(cinema), Times.Once);
        }

        /// <summary>
        /// Test that when calling UpdateCinema it exectue once.
        /// </summary>
        [Test]
        public void UpdateCinemaShouldBeCalledOnce()
        {
            Mock<ICinemaRepository> mockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:25", out TimeSpan length);
            Cinema cinema = new Cinema()
            {
                Name = "Test Cinema",
                Address = "Budapest, some street 14",
                NumberOfTheaters = 0,
                TicketPrice = 12,
            };
            cinemaLogic.UpdateCinema(cinema);
            mockedRepo.Verify(repo => repo.Update(cinema), Times.Once);
        }

        // Non-CRUD Tests:

        /// <summary>
        /// Test that GetCinemaWithMostScreeningsOptionsThisWeek returns only screenings in the next 7 days.
        /// </summary>
        [Test]
        public void GetCinemaWithMostScreeningsOptionsThisWeekShouldReturnOnlyScreeningsInTheNext7Days()
        {
            Mock<ICinemaRepository> cinemaMockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            Mock<IScreeningRepository> screeningMockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            List<Screening> screenings = new List<Screening>
            {
                new Screening
                {
                    Id = 1,
                    TheaterId = 1,
                    MovieId = 2,
                    StartTime = DateTime.Now.AddHours(4),
                    TicketsSold = 65,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 2,
                    TheaterId = 1,
                    MovieId = 4,
                    StartTime = DateTime.Now.AddDays(2),
                    TicketsSold = 22,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 1,
                    MovieId = 1,
                    StartTime = DateTime.Now.AddDays(6).AddHours(23),
                    TicketsSold = 0,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 4,
                    TheaterId = 1,
                    MovieId = 7,
                    StartTime = DateTime.Now.AddDays(8),
                    TicketsSold = 0,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 5,
                    TheaterId = 1,
                    MovieId = 5,
                    StartTime = DateTime.Now.AddDays(7).AddHours(1),
                    TicketsSold = 0,
                    CinemaId = 1,
                },
            };

            List<Cinema> cinemas = new List<Cinema>
            {
                new Cinema
                {
                    Id = 1,
                    Name = "Cinema City",
                    Address = "Budapest",
                    NumberOfTheaters = 5,
                    Theaters = new List<Theater>
                    {
                        new Theater
                        {
                            Id = 1,
                            Capacity = 100,
                            CinemaId = 1,
                            Number = 1,
                            NumberOfRows = 12,
                            ScreenSize = 112,
                        },
                    },
                    Screenings = new List<Screening>
                    {
                        screenings.ElementAt(0),
                        screenings.ElementAt(1),
                        screenings.ElementAt(2),
                        screenings.ElementAt(3),
                        screenings.ElementAt(4),
                    },
                },
            };

            cinemaMockedRepo.Setup(repo => repo.GetAll()).Returns(cinemas.AsQueryable);
            screeningMockedRepo.Setup(repo => repo.GetAll()).Returns(screenings.AsQueryable);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, cinemaMockedRepo.Object, screeningMockedRepo.Object);

            var result = cinemaLogic.GetCinemaWithMostScreeningsOptionsThisWeek();
            Assert.That(result.Screenings.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Test that GetCinemaWithMostScreeningsOptionsThisWeek returns the cinema with the most screenings in the next 7 days.
        /// </summary>
        [Test]
        public void GetCinemaWithMostScreeningsOptionsThisWeekShouldReturnTheCinemaWithMostScreeningsInTheNext7Days()
        {
            Mock<ICinemaRepository> cinemaMockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            Mock<IScreeningRepository> screeningMockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            List<Screening> screenings = new List<Screening>
            {
                new Screening
                {
                    Id = 1,
                    TheaterId = 1,
                    MovieId = 2,
                    StartTime = DateTime.Now.AddHours(4),
                    TicketsSold = 65,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 2,
                    TheaterId = 2,
                    MovieId = 4,
                    StartTime = DateTime.Now.AddDays(2),
                    TicketsSold = 22,
                    CinemaId = 2,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 2,
                    MovieId = 1,
                    StartTime = DateTime.Now.AddDays(6).AddHours(23),
                    TicketsSold = 0,
                    CinemaId = 2,
                },
                new Screening
                {
                    Id = 4,
                    TheaterId = 1,
                    MovieId = 7,
                    StartTime = DateTime.Now.AddDays(8),
                    TicketsSold = 0,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 5,
                    TheaterId = 1,
                    MovieId = 5,
                    StartTime = DateTime.Now.AddDays(7).AddHours(1),
                    TicketsSold = 0,
                    CinemaId = 1,
                },
            };

            List<Cinema> cinemas = new List<Cinema>
            {
                new Cinema
                {
                    Id = 1,
                    Name = "Cinema City",
                    Address = "Budapest",
                    NumberOfTheaters = 5,
                    Theaters = new List<Theater>
                    {
                        new Theater
                        {
                            Id = 1,
                            Capacity = 100,
                            CinemaId = 1,
                            Number = 1,
                            NumberOfRows = 12,
                            ScreenSize = 112,
                        },
                    },
                    Screenings = new List<Screening>
                    {
                        screenings.ElementAt(0),
                        screenings.ElementAt(3),
                        screenings.ElementAt(4),
                    },
                },
                new Cinema
                {
                    Id = 2,
                    Name = "Globus Max",
                    Address = "Budapest",
                    NumberOfTheaters = 7,
                    Theaters = new List<Theater>
                    {
                        new Theater
                        {
                            Id = 2,
                            Capacity = 120,
                            CinemaId = 2,
                            Number = 1,
                            NumberOfRows = 15,
                            ScreenSize = 120,
                        },
                    },
                    Screenings = new List<Screening>
                    {
                        screenings.ElementAt(1),
                        screenings.ElementAt(2),
                    },
                },
            };

            cinemaMockedRepo.Setup(repo => repo.GetAll()).Returns(cinemas.AsQueryable);
            screeningMockedRepo.Setup(repo => repo.GetAll()).Returns(screenings.AsQueryable);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, cinemaMockedRepo.Object, screeningMockedRepo.Object);

            var result = cinemaLogic.GetCinemaWithMostScreeningsOptionsThisWeek();
            Assert.That(result.Cinema.Id, Is.EqualTo(2));
        }

        /// <summary>
        /// Test that GetMonthlyProfitForAllCinemas returns the correct amount of past month screenings profit.
        /// </summary>
        [Test]
        public void GetMonthlyProfitForAllCinemasShouldReturnCorrectAmountOfProfit()
        {
            Mock<ICinemaRepository> cinemaMockedRepo = new Mock<ICinemaRepository>(MockBehavior.Loose);
            Mock<IScreeningRepository> screeningMockedRepo = new Mock<IScreeningRepository>(MockBehavior.Loose);
            List<Screening> screenings = new List<Screening>
            {
                new Screening
                {
                    Id = 1,
                    TheaterId = 1,
                    MovieId = 2,
                    StartTime = DateTime.Now.AddHours(-10),
                    TicketsSold = 100,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 2,
                    TheaterId = 1,
                    MovieId = 4,
                    StartTime = DateTime.Now.AddDays(-15),
                    TicketsSold = 22,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 3,
                    TheaterId = 1,
                    MovieId = 1,
                    StartTime = DateTime.Now.AddDays(-4).AddHours(-12),
                    TicketsSold = 3,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 4,
                    TheaterId = 1,
                    MovieId = 7,
                    StartTime = DateTime.Now.AddDays(1),
                    TicketsSold = 20,
                    CinemaId = 1,
                },
                new Screening
                {
                    Id = 5,
                    TheaterId = 1,
                    MovieId = 5,
                    StartTime = DateTime.Now.AddMonths(-1).AddHours(-1),
                    TicketsSold = 10,
                    CinemaId = 1,
                },
            };

            List<Cinema> cinemas = new List<Cinema>
            {
                new Cinema
                {
                    Id = 1,
                    Name = "Cinema City",
                    Address = "Budapest",
                    NumberOfTheaters = 5,
                    TicketPrice = 10,
                    Theaters = new List<Theater>
                    {
                        new Theater
                        {
                            Id = 1,
                            Capacity = 100,
                            CinemaId = 1,
                            Number = 1,
                            NumberOfRows = 12,
                            ScreenSize = 112,
                        },
                    },
                    Screenings = new List<Screening>
                    {
                        screenings.ElementAt(0),
                        screenings.ElementAt(1),
                        screenings.ElementAt(2),
                        screenings.ElementAt(3),
                        screenings.ElementAt(4),
                    },
                },
            };
            cinemaMockedRepo.Setup(x => x.GetAll()).Returns(cinemas.AsQueryable);
            screeningMockedRepo.Setup(x => x.GetAll()).Returns(screenings.AsQueryable);
            ICinemaLogic cinemaLogic = new CinemaLogic(null, cinemaMockedRepo.Object, screeningMockedRepo.Object);

            var result = cinemaLogic.GetMonthlyProfitForAllCinemas();
            Assert.That(result.FirstOrDefault().MonthlyProfit, Is.EqualTo(1250));
        }
    }
}
