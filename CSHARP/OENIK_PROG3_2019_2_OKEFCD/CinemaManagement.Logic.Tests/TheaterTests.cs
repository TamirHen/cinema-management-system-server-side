﻿namespace CinemaManagement.Logic.Tests
{
    using System;
    using CinemaManagement.Data;
    using CinemaManagement.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for all the TheaterLogic tests.
    /// </summary>
    [TestFixture]
    public class TheaterTests
    {
        /// <summary>
        /// Test that when calling GetTheaterById it exectue once.
        /// </summary>
        /// <param name="id">Id of wanted theater.</param>
        [Test]
        [TestCase(1)]
        [TestCase(-1)]
        public void GetTheaterByIdShouldBeCalledOnce(int id)
        {
            Mock<ITheaterRepository> mockedRepo = new Mock<ITheaterRepository>(MockBehavior.Loose);
            ITheaterLogic theaterLogic = new TheaterLogic(null, mockedRepo.Object);
            var theaterList = theaterLogic.GetTheaterById(id);
            mockedRepo.Verify(repo => repo.GetOneById(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Test that when calling GetAllTheaters it exectue once.
        /// </summary>
        [Test]
        public void GetAllTheatersShouldBeCalledOnce()
        {
            Mock<ITheaterRepository> mockedRepo = new Mock<ITheaterRepository>(MockBehavior.Loose);
            ITheaterLogic theaterLogic = new TheaterLogic(null, mockedRepo.Object);
            var theaterList = theaterLogic.GetAllTheaters();
            mockedRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test that when calling CreateTheater it execute once.
        /// </summary>
        [Test]
        public void CreateTheaterShouldBeCalledOnce()
        {
            Mock<ITheaterRepository> mockedRepo = new Mock<ITheaterRepository>(MockBehavior.Loose);
            ITheaterLogic theaterLogic = new TheaterLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:30", out TimeSpan length);
            Theater theater = new Theater
            {
                Number = 1,
                Capacity = 150,
                NumberOfRows = 12,
                ScreenSize = 113,
            };
            theaterLogic.CreateTheater(theater);
            mockedRepo.Verify(repo => repo.Create(theater), Times.Once);
        }

        /// <summary>
        /// Test that when calling DeleteTheater it execute once.
        /// </summary>
        [Test]
        public void DeleteTheaterShouldBeCalledOnce()
        {
            Mock<ITheaterRepository> mockedRepo = new Mock<ITheaterRepository>(MockBehavior.Loose);
            ITheaterLogic theaterLogic = new TheaterLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("01:55", out TimeSpan length);
            Theater theater = new Theater()
            {
                Number = 2,
                Capacity = 200,
                NumberOfRows = 15,
                ScreenSize = 125,
            };
            theaterLogic.DeleteTheater(theater);
            mockedRepo.Verify(repo => repo.Delete(theater), Times.Once);
        }

        /// <summary>
        /// Test that when calling UpdateTheater it exectue once.
        /// </summary>
        [Test]
        public void UpdateTheaterShouldBeCalledOnce()
        {
            Mock<ITheaterRepository> mockedRepo = new Mock<ITheaterRepository>(MockBehavior.Loose);
            ITheaterLogic theaterLogic = new TheaterLogic(null, mockedRepo.Object);
            TimeSpan.TryParse("02:25", out TimeSpan length);
            Theater theater = new Theater()
            {
                Number = 3,
                Capacity = 180,
                NumberOfRows = 18,
                ScreenSize = 120,
            };
            theaterLogic.UpdateTheater(theater);
            mockedRepo.Verify(repo => repo.Update(theater), Times.Once);
        }
    }
}
