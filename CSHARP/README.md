# OENIK_PROG3_2019_2_OKEFCD

Tamir Hen's project for programming 3.

Title: Cinema Management System.

Consule application functionality: 
		The program is seperated to 3 modes.
			1. View mode - Display all the cinemas details:
				a. List all movies.
				b. List all cinemas:
					- List all details of each cinema.
					- List all theaters of each cinema.
					- List all screenings playing at each cinema.
				e. Show all screenings divided by movies.
				f. Show the cinema with the most screening options this week.
				g. Show Monthly profit for each cinema.
				
			2. Edit mode:
				Add/Modify/Remove objects (cinema,theater,movie,screening).
				
			3. Generate movies:
				Send a request to the java endpoint end gets 101 new generated movies into the DB.