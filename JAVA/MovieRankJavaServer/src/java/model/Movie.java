/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author tamirhen
 */
public class Movie {
    
    public Movie() {
        
    }
    
    private String name;
    private double rank;
    private int lengthInMinutes;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public int getLength() {
        return this.lengthInMinutes;
    }

    public void setLength(int length) {
        this.lengthInMinutes = length;
    }
}