/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.Movie;

/**
 *
 * @author tamirhen
 */
public class MovieGenerator {
    
    public MovieGenerator(){
        
    }
    
    public List<Movie> createMovies(int numOfMovieToCreate){
        List<Movie> movies = new ArrayList<Movie>();
        Random rand = new Random();
        for (int i = 0; i < numOfMovieToCreate; i++) {
            Movie movie = new Movie();
            movie.setRank(5 + 5 * rand.nextDouble()); // Set random number in range (5.0 - 10.0)
            movie.setName("Movie number " + (i+1));
            movie.setLength(90 + rand.nextInt(90)); // Set random number in range (90 - 180)
            movies.add(movie);
        }
        return movies;
    }
}
